# Maemo and MeeGo SDK installers I wrote while working for Nokia

## maemo-sdk-installer

Installer for the Maemo SDK+, which was experimental, Scratchbox 2 based, SDK for Fremantle.

## maemo-sdk-install-wizard

PyQt GUI front-end to the Scratchbox & the SDK command line installer scripts for the Fremantle SDK (Maemo 5).

## harmattan-sdk-setup

The SDK installer of the Harmattan OS used in Nokia N9 device. Uses the same UI front-end as the Fremantle installer (maemo-sdk-install-wizard). Enhancement from the Fremantle installer is that both of the shell script Scratchbox & SDK installers are rewritten to Python too. So unlike Fremanle, there is only one installer for the Harmattan SDK.
