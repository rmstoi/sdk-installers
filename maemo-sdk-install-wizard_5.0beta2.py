#!/usr/bin/env python

'''GUI (PyQt4) Installer for Scratchbox1 based Maemo SDK'''

# This file is part of Maemo SDK
#
# Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: ruslan.mstoi@nokia.com
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 2 of the License, or (at your option) any later
# version. This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details. You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import re
import sys
import pwd
import grp
import time
import urllib
import signal
import traceback
import subprocess
from optparse import OptionParser

try:
    from PyQt4 import QtGui
    from PyQt4 import QtCore
except ImportError, e:
    # Can raise AttributeError if the version of PyQt4 is prior to Qt 4.3,
    # since according to
    # http://doc.trolltech.com/4.4/porting4-overview.html#wizard-dialogs-qwizard
    # QWizard and siblings were added to that version of Qt4. Systems that have
    # too old Qt and bindings by default: Debian Etch.
    print "Python Qt4 bindings are not found!\n"
    print ("This script needs at least version 4.3 of Python Qt4 bindings.\n"
           "Please install those bindings (on Debian systems it is "
           "python-qt4\npackage, on Fedora it is PyQt4 package) then try "
           "again.")
    sys.exit(1)

# command line options and arguments
OPTIONS = None
ARGS = None
OPT_PARSER = None

# logger, initialized in main
LOG = None

# names shown to the user (what is installed/removed and installer's name)
PRODUCT_NAME = 'Maemo %s SDK' % ("5.0beta2")
PRODUCT_NAME_SHORT = 'SDK'
SB_NAME = 'Scratchbox'

MY_NAME = '%s Installer' % PRODUCT_NAME
MY_VERSION = "0.1.1 ($Revision: 1078 $)"

# SDK time & space consumption: these are very rough
INST_CONS_TIME = '20 minutes'
INST_CONS_SPACE = "2GB"

# where scratchbox is found
SB_PATH = "/scratchbox"

# URL's of installers (can be set to local file)
SB_INSTALLER_URL = "http://repository.maemo.org/unstable/5.0beta/maemo-scratchbox-install_5.0beta.sh"
SDK_INSTALLER_URL = "http://repository.maemo.org/unstable/5.0beta/maemo-sdk-install_5.0beta.sh"

# default target names for this release of SDK, user can choose different
# prefix if default targets already exist in scratchbox
TARGET_PREFIX = "FREMANTLE"
TARGET_POSTFIX_X86 = "_X86"
TARGET_POSTFIX_ARMEL = "_ARMEL"
TARGET_X86 = TARGET_PREFIX + TARGET_POSTFIX_X86
TARGET_ARMEL = TARGET_PREFIX + TARGET_POSTFIX_ARMEL

# license text for the SDK (non-ASCII quotes replaced with ASCII)
SDK_LICENSE = \
'''1) IMPORTANT: READ CAREFULLY BEFORE INSTALLING, DOWNLOADING, OR USING THE
SOFTWARE DEVELOPMENT KIT ("SDK" AS DEFINED BELOW) AND/OR SOFTWARE INCLUDED INTO
THE SDK

2) The SDK comprises of a) some software copyrighted by Nokia Corporation or
third parties in binary form (collectively "Licensed Software") and/or b) Open
Source Software in binary and source code form.

3) Licensed Software (including, without limitation, the downloading,
installation and/or the use thereof) is subject to, and licensed to you under,
the Nokia Software Development Kit Agreement, which you have to accept if you
choose to download the Licensed Software. Licensed Software is distributed to
you only in binary form.

4) The SDK is provided to you "AS IS" and Nokia, its affiliates and/or its
licensors do not make any representations or warranties, express or implied,
including, without any limitation, the warranties of merchantability or
fittness for a particular purpose, or that the SDK will not infringe any any
third party patents, copyrights, trademarks or other rights, or that the SDK
will meet your requirements or that the operation of the SDK will be
uninterrupted and/or error-free. By downloading and/or using the SDK you accept
that installation, any use and the results of the installation and/or happens
and is solely at your own risk and that Nokia assumes no liability whatsoever
for any damages that you may incur or suffer in connection with the SDK and/or
the installation or use thereof.

5) The Open Source Software is licensed and distributed under the GNU General
Public License (GPL), the GNU lesser General Public License (LGPL, aka. The GNU
Library General Public License) and/or other copyright licenses, permissions,
notices or disclaimers containing obligation or permission to provide the
source code of such software with the binary / executable form delivery of the
said software. Any source code of such software that is not part of this
delivery is made available to you in accordance with the referred license terms
and conditions on http://www.maemo.org. Alternatively, Nokia offers to provide
any such source code to you on CD-ROM for a charge covering the cost of
performing such distribution, such as the cost of media, shipping and handling,
upon written request to Nokia at:

Source Code Requests
Nokia Corporation
P.O.Box 407
FIN00045 Nokia Group
Finland.

This offer is valid for a period of three (3) years.

The exact license terms of GPL, LGPL and said certain other licenses, as well
as the required copyright and other notices, permissions and acknowledgements
are reproduced in and delivered to you as part of the referred source code.
'''

def get_all_usernames():
    '''Returns non-system usernames if can get the system limits for normal
    user's UID. If not then returns all of the usernames in the system. The
    usernames are returned in a list.'''
    user_conf_file = "/etc/adduser.conf"

    # inclusive range of UIDs for normal (non-system) users
    # by default this is the maximum possible range, that includes all users
    first_uid = 0
    last_uid = sys.maxint

    found_first_uid = False
    found_last_uid = False 

    # try to get the UID range
    if os.path.isfile(user_conf_file):
        for l in open(user_conf_file):

            l = l.strip()

            if l.startswith("FIRST_UID"):
                first_uid = int(l.split("=")[1])
                found_first_uid = True

            if l.startswith("LAST_UID"):
                last_uid = int(l.split("=")[1])
                found_last_uid = True

            # both found
            if found_first_uid and found_last_uid:
                break

    usernames = []

    for i in pwd.getpwall():
        # if UID not within the range, then skip this user
        if not first_uid <= i.pw_uid <= last_uid:
            continue
        usernames.append(i.pw_name)

    usernames.sort()

    return usernames

def get_default_username():
    '''Returns username of the user that invoked sudo or su to run this
    script. Returns None if couldn't get such entry.'''
    pwd_ent = None

    # try to get the user that ran sudo
    username = os.getenv('SUDO_USER')

    # or user that ran su
    if not username:
        username = os.getenv('USERNAME')

    if username:

        # installing SDK as root is not allowed, also the list of users does
        # not show system users
        if username == 'root':
            LOG("Omitting default user 'root'")
            return None

        # verify that this user actually exists
        try:
            pwd_ent = pwd.getpwnam(username)
        except KeyError:
            LOG("User %s not in password file!" % username)
            return None

    return username

def parse_options():
    '''Parses the command line options'''
    global OPTIONS
    global ARGS
    global OPT_PARSER

    usage = 'Usage: %prog [options]' + \
'''

  %s.

  Installs %s.''' % (MY_NAME, PRODUCT_NAME)

    OPT_PARSER = OptionParser(usage = usage, version = "%prog " + MY_VERSION)

    (OPTIONS, ARGS) = OPT_PARSER.parse_args()


def download_file(url, chown_username = None, set_exec_bit = True):
    '''Downloads a file into temporary location. Returns that location.
    url = URL to download from
    chown_username = if set, the ownership of the downloaded file will be
                     given to this user
    set_exec_bit = if True execute mode bit of the downloaded file will be
                   set'''

    LOG("Downloading %s" % url)
    filename, headers = urllib.urlretrieve(url)
    LOG("Saved download into %s" % filename)

    if set_exec_bit:
        os.chmod(filename, 0700)

    if chown_username:
        pwd_ent = pwd.getpwnam(chown_username)
        os.chown(filename, pwd_ent.pw_uid, pwd_ent.pw_gid)

    return filename

def bool_to_yesno(bool_value):
    '''Return yes if Boolean value is true, no if it is false'''
    if bool_value:
        return 'Yes'
    else:
        return 'No'

def file_append_lines(filename, lines):
    '''Appends list of lines into a file. Newlines are added to each appended
    line.
    filename = name of the file
    lines = list of lines'''

    if not os.path.isfile(filename):
        LOG("WARNING! Appending to non-existing file (%s), will be created!" %
            (filename))

    LOG("Appending into file %s lines %s" % (filename, lines))

    fo = open(filename, "a")

    try:
        for line in lines:
            fo.write(line + '\n')

    finally:
        fo.close()

def set_guid(pwd_ent, set_sbox_gid = False):
    '''Changes the effective, real and saved set-user-ID user and group IDs,
    should be used to permanently drop root privileges.  Also sets HOME
    environment variable since maemo-sdk command extensively uses that
    variable.

    pwd_ent = Password database entry of the user whose credentials will be
    used
    set_sbox_gid = The GID will be set to that of the sbox. This is required
                   to run the SDK installer and any other scratchbox related
                   command. sg cannot be used since it does not return the
                   exit status of the executed process. Another option is
                   newgrp command.'''

    if set_sbox_gid:
        gid = grp.getgrnam("sbox").gr_gid
    else:
        gid = pwd_ent.pw_gid

    os.setgid(gid)
    os.setuid(pwd_ent.pw_uid)
    os.environ['HOME'] = pwd_ent.pw_dir
    os.environ['USER'] = pwd_ent.pw_name # SDK installer needs this

class CmdExecError(Exception):
    '''Exception raised when execution of a command fails.'''
    pass

def exec_cmd(command, username = None, set_sbox_gid = False, send_text = None):
    '''Executes a command and raises exception if command fails.
    username = if set, will run command with the credentials (UID & GID) of
               that user.
    set_sbox_gid = sets the GID of executed command to sbox, so that scratchbox
                   commands can be executed. This is naturally only used when
                   the username is specified, since root is not allowed to run
                   scratchbox commands.
    send_text = Text to send to stdin of the process before waiting for it'''
    pwd_ent = None

    if username:
        LOG("Executing as user %s: %s" % (username, command))
        child_preexec_fn = lambda: set_guid(pwd.getpwnam(username),
                                            set_sbox_gid)
    else:
        LOG("Executing: %s" % (command,))
        child_preexec_fn = None

    if send_text:
        p_stdin = subprocess.PIPE
    else:
        p_stdin = None # as default in Popen

    p = subprocess.Popen(command,
                         stdin = p_stdin,
                         stdout = LOG.fo_log,
                         stderr = subprocess.STDOUT,
                         preexec_fn = child_preexec_fn,
                         shell = True)
    if send_text:
        p.communicate(send_text)

    p.wait()

    if p.returncode:
        raise CmdExecError("Giving up, because failed to: %s" % command)

def scratchbox_target_exists(username, target):
    '''Returns True if specified target for specified username exists in
    scratchbox.'''
    return os.path.isdir("%s/users/%s/targets/%s" %
                         (SB_PATH, username, target))

def scratchbox_prefix_exist(username, prefix):
    '''Returns True if either armel or x86 target with specified prefix exists
    in scratchbox. The SDK installer creates targets by taking prefix and
    appending architecture.'''

    return scratchbox_target_exists(username, prefix + TARGET_POSTFIX_X86) or \
        scratchbox_target_exists(username, prefix + TARGET_POSTFIX_ARMEL)

class Logger(object):
    '''Class for logging.'''

    def __init__(self):
        '''Constructor'''

        script_name = os.path.basename(sys.argv[0])

        # file name of the log file: script name, with extenstion replaced
        self.fn_log = "/tmp/%s.log" % script_name[:script_name.rfind('.')]

        self.fo_log = None

        try:
            self.fo_log = open(self.fn_log, 'w')
        except:
            print ("Could not open log file %s!" % (self.fn_log,))
            raise

        self.log("Python version: %s" % repr(sys.version))
        self.log("Installer version: %s" % MY_VERSION)

    def __del__(self):
        '''Destructor'''
        if self.fo_log:
            self.fo_log.close()

    def log(self, msg):
        """Writes a log message."""
        self.fo_log.write("V [%s]: %s\n" % 
                          (time.strftime("%H:%M:%S %d.%m.%Y"), msg))
        self.fo_log.flush()

    def __call__(self, *args, **kwds):
        '''Shortcut for log'''
        self.log(*args, **kwds)

    def log_exc(self):
        '''Writes current exception information into the log file'''
        fmt = '-' * 5 + " %s " + '-' * 5

        self.log(fmt % "Begin logging exception")
        traceback.print_exc(file = self.fo_log)
        self.log(fmt % "End logging exception")

class HostInfo(object):
    '''Information about host'''

    def __init__(self):
        '''Constructor'''

        # whether host already has scratchbox installed
        self.__has_scratchbox = os.path.isdir(SB_PATH)

        self.__scratchbox_op_name = self.__get_scratchbox_op_name()

        # whether current VDSO settings are unsupported
        self.__has_unsupported_vdso = self.__check_vdso_unsupported()

        LOG("Got host info: %s" % self)

    @property
    def has_scratchbox(self):
        return self.__has_scratchbox

    @property
    def scratchbox_op_name(self):
        return self.__scratchbox_op_name

    @property
    def has_unsupported_vdso(self):
        return self.__has_unsupported_vdso

    def __check_vdso_unsupported(self):
        '''Returns True if current VDSO setting is not supported by
        scratchbox'''
    
        vdso_str = subprocess.Popen(["sysctl", "-n", "vm.vdso_enabled"],
                                    stdout = subprocess.PIPE
                                    ).communicate()[0].strip()

        LOG("Found current VDSO value: %s" % vdso_str)

        supported_vdso_list = [0, 2]

        return not int(vdso_str) in supported_vdso_list

    def __get_scratchbox_op_name(self):
        '''Returns string of what operation is needed to get scratchbox to this
        host: install or upgrade'''
        op_name = "Unknown"

        if self.has_scratchbox:
            return "Upgrade"

        else:
            return "Install"

    def __str__(self):
        '''String representation method.'''
        return str(self.__dict__)

# field names for wizard and its siblings
FNLicenseAccept       = "license_accept"       # bool
FNInstallScratchbox   = "install_scratchbox"   # bool
FNInstallSDK          = "install_sdk"          # bool
FNSelectedUsername    = "selected_username"    # string
FNTargetX86Exist      = "target_x86_exist"     # bool
FNTargetArmelExist    = "target_armel_exist"   # bool
FNRemoveTargets       = "targets_remove"       # bool
FNTargetPrefix        = "targets_prefix"       # string
FNSDKInstMOptArg      = "sdk_m_opt_arg"        # string
FNSDKInstMOptArgText  = "sdk_m_opt_arg_txt"    # string
FNVDSOSetPerm         = "vdso_set_permanently" # bool
FNInstallNokiaBins    = "nokiabins_install"    # bool
FNNokiaBinsRepo       = "nokiabins_repo"       # string

class IntroPage(QtGui.QWizardPage):
    '''Introduction page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("Welcome to the %s installation wizard" % PRODUCT_NAME)

        intro_label = QtGui.QLabel("This application will guide you through " 
                                   "installation of the %s. Installation will "
                                   "take about %s and %s of your disk space."
                                   % (PRODUCT_NAME, INST_CONS_TIME,
                                      INST_CONS_SPACE))
        intro_label.setWordWrap(True)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(intro_label)

        self.setLayout(layout)

class LicensePage(QtGui.QWizardPage):
    '''EUSA page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("License for %s" % PRODUCT_NAME)
        self.setSubTitle("To install %s you will have to read and accept the "
                         "terms of Nokia EULA (End User License Agreement)." %
                         PRODUCT_NAME)

        license_text_edit = QtGui.QTextEdit()
        license_text_edit.setPlainText(SDK_LICENSE)
        license_text_edit.setReadOnly(True)

        txt = "%s License. Use Ctrl+Wheel to zoom the text." % \
            (PRODUCT_NAME_SHORT)
        license_text_edit.setToolTip(txt)
        license_text_edit.setWhatsThis(txt)

        accept_check_box = QtGui.QCheckBox("I &accept the terms of the license")
        accept_check_box.setCheckState(QtCore.Qt.Unchecked)

        self.registerField(FNLicenseAccept + '*', accept_check_box)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(license_text_edit)
        layout.addWidget(accept_check_box)
        self.setLayout(layout)

class InstallOptsPage(QtGui.QWizardPage):
    '''Installation options page'''

    def __init__(self, host_has_scratchbox, scratchbox_op_name):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("Installation options")
        self.setSubTitle("Please choose what to install.")

        if host_has_scratchbox:
            info_str = ("A previous %s installation has been detected "
                        "in %s. You have an option to upgrade that "
                        "installation." % (SB_NAME, SB_PATH))
        else:
            info_str = ("Previous installation of %(sb_name)s was not "
                        "detected. This installer can only detect previous "
                        "installation of %(sb_name)s in %(sb_path)s. If you "
                        "have previously installed %(sb_name)s in some other "
                        "path, new version of %(sb_name)s will be installed "
                        "in %(sb_path)s by this installer." %
                        { "sb_name" : SB_NAME, "sb_path" : SB_PATH})

        info_label = QtGui.QLabel(info_str)
        info_label.setWordWrap(True)

        self.install_sb_checkbox = QtGui.QCheckBox(
            scratchbox_op_name + " &" + SB_NAME)
        self.install_sb_checkbox.setChecked(True)

        # if sb not installed, then must install, user has no choice
        if not host_has_scratchbox:
            self.install_sb_checkbox.setEnabled(False)

        self.install_sdk_checkbox = QtGui.QCheckBox(
            "Install &%s" % PRODUCT_NAME)
        self.install_sdk_checkbox.setChecked(True)

        self.registerField(FNInstallScratchbox, self.install_sb_checkbox)
        self.registerField(FNInstallSDK, self.install_sdk_checkbox)

        self.connect(self.install_sb_checkbox, QtCore.SIGNAL("toggled(bool)"),
                     self, QtCore.SIGNAL("completeChanged()"))

        self.connect(self.install_sdk_checkbox, QtCore.SIGNAL("toggled(bool)"),
                     self, QtCore.SIGNAL("completeChanged()"))

        layout = QtGui.QVBoxLayout()
        layout.addWidget(info_label)
        layout.addWidget(self.install_sb_checkbox)
        layout.addWidget(self.install_sdk_checkbox)
        self.setLayout(layout)

    def isComplete(self):
        '''Overrides the method of QWizardPage, to disable next button if
        nothing is selected for installation.'''
        return self.install_sb_checkbox.isChecked() or \
            self.install_sdk_checkbox.isChecked()

class UsersPage(QtGui.QWizardPage):
    '''User selection page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("Users")
        self.setSubTitle("Please select the user to install %s for." %
                         PRODUCT_NAME)

        all_usernames = get_all_usernames()
        default_username = get_default_username()

        if not default_username: # in case default not found use first one
            default_username = all_usernames[0]

        users_list_widget = QtGui.QListWidget()

        txt = ("List of users on the system. %s will be installed for the "
               "selected user." % PRODUCT_NAME)
        users_list_widget.setToolTip(txt)
        users_list_widget.setWhatsThis(txt)

        # will be set in following signal handler
        self.__selected_username = None
        self.__target_x86_exist = False # targets for selected user
        self.__target_armel_exist = False

        self.connect(users_list_widget,
                     QtCore.SIGNAL("currentTextChanged(const QString &)"),
                     self.selectedUsernameChanged)

        # add usernames to the list
        for index, username in enumerate(all_usernames):
            users_list_widget.addItem(username)

            if username == default_username:
                users_list_widget.setCurrentRow(index)

        self.registerField(FNSelectedUsername, self, "selectedUsername")
        self.registerField(FNTargetX86Exist, self, "targetX86Exist")
        self.registerField(FNTargetArmelExist, self, "targetArmelExist")

        layout = QtGui.QVBoxLayout()
        layout.addWidget(users_list_widget)
        self.setLayout(layout)

    def getSelectedUsername(self):
        '''Returns selected username'''
        return self.__selected_username

    selectedUsername = QtCore.pyqtProperty("QString", getSelectedUsername)

    targetX86Exist = QtCore.pyqtProperty(
        "bool", lambda self: self.__target_x86_exist)
    targetArmelExist = QtCore.pyqtProperty(
        "bool", lambda self: self.__target_armel_exist)

    def selectedUsernameChanged(self, new_name):
        '''Signal handler for list widgets currentTextChanged. Sets the
        selected username to the current selection. Also updates related
        info.'''

        # segmentation faults without cast
        self.__selected_username = QtCore.QString(new_name)

        self.__target_x86_exist = scratchbox_target_exists(
            self.__selected_username, TARGET_X86)
        self.__target_armel_exist = scratchbox_target_exists(
            self.__selected_username, TARGET_ARMEL)

class TargetsPage(QtGui.QWizardPage):
    '''Targets page, shown only if targets exist for selected user.'''

    def __init__(self):
        '''Constructor'''
        QtGui.QWizardPage.__init__(self)

        self.setTitle("Targets")
        # subtitle is set in initializePage

        self.remove_targets_checkbox = QtGui.QCheckBox(
            "&Remove existing targets")

        self.group_box = QtGui.QGroupBox(
            "To &create new targets using different name prefix:")
        #self.group_box.setFlat(True)

        info_str = ("Please specify prefix to be used with target names. "
                    "<i>Specified prefix should not be the same as that of "
                    "the currently existing targets (e.g. %s).</i> "
                    "When targets will be created, architecture (x86 or "
                    "armel) will be added to the prefix automatically." % 
                    (TARGET_PREFIX))

        info_label = QtGui.QLabel(info_str)
        info_label.setWordWrap(True)

        self.target_prefix_line_edit = QtGui.QLineEdit()

        group_box_layout = QtGui.QVBoxLayout()
        group_box_layout.addStretch()
        group_box_layout.addWidget(info_label)
        group_box_layout.addWidget(self.target_prefix_line_edit)

        self.group_box.setLayout(group_box_layout)

        self.connect(self.remove_targets_checkbox,
                     QtCore.SIGNAL("toggled(bool)"),
                     self.removeTargetsCheckboxToggled)

        self.remove_targets_checkbox.setChecked(False)

        self.registerField(FNRemoveTargets, self.remove_targets_checkbox)
        self.registerField(FNTargetPrefix, self.target_prefix_line_edit)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.remove_targets_checkbox)
        layout.addStretch()
        layout.addWidget(self.group_box)
        self.setLayout(layout)

    def initializePage(self):
        '''Overrides the method of QWizardPage, to initialize the page with
        some dynamic text.'''

        target_x86_exist = self.field(FNTargetX86Exist).toBool()
        target_armel_exist = self.field(FNTargetArmelExist).toBool()
        selected_username = self.field(FNSelectedUsername).toString()

        assert target_armel_exist or target_x86_exist, \
            "At least one target should exist for this page to be usable!"

        # both exist
        if target_x86_exist and target_armel_exist:
            targets_str = "%s and %s targets already exist" % \
                (TARGET_X86, TARGET_ARMEL)

        # either one exist
        else:
            if target_x86_exist:
                targets_str = "%s" % TARGET_X86
            elif target_armel_exist:
                targets_str = "%s" % TARGET_ARMEL
            targets_str += " target already exists"

        self.setSubTitle("%s for user %s. Please choose appropriate action." %
                         (targets_str, selected_username))

        # set the line edits text to some non-existing target prefix
        nonexistent_prefix = TARGET_PREFIX + time.strftime("_%Y%m%d")
        while True:
            if scratchbox_prefix_exist(selected_username, nonexistent_prefix):
                nonexistent_prefix += "_"
            else:
                break
        
        self.target_prefix_line_edit.setText(nonexistent_prefix)

    def validatePage(self):
        '''Overrides the method of QWizardPage, verify valid input from the
        user'''

        # not removing targets, verify that prefix is not used in sb
        if not self.remove_targets_checkbox.isChecked():
            selected_username = self.field(FNSelectedUsername).toString()

            # prefix exist
            if scratchbox_prefix_exist(selected_username,
                                       self.target_prefix_line_edit.text()):
                QtGui.QMessageBox.critical(
                    self,
                    "Target exist!",
                    "Target with prefix '%s' exist in %s for "
                    "user %s, please specify different prefix!" %
                    (self.target_prefix_line_edit.text(), SB_NAME,
                     selected_username))
                return False

        return True

    def removeTargetsCheckboxToggled(self, on):
        '''Handler for toggled signal of remove_targets_checkbox . Enables
        target name prefix group box if checkbox is unchecked, and disables the
        group box otherwise. When the group box is enabled the focus is set to
        line edit'''
        self.group_box.setDisabled(on)

        if not on: # group box is enabled
            self.target_prefix_line_edit.setFocus()

class PkgPage(QtGui.QWizardPage):
    '''SDK package selection page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("Package selection")
        self.setSubTitle("Please select packages to install.")

        # UI text and respective -m option argument of the SDK installer
        self.__txt2opt = [
            ["&Minimal rootstrap only", "none"],
            ["&Runtime environment", "maemo-sdk-runtime"],
            ["Runtime environment + all &dev packages", "maemo-sdk-dev"],
            ["Runtime environment + all dev + db&g packages", "maemo-sdk-debug"]
        ]

        default_btn_index = 2 # checked by default
    
        self.button_group = QtGui.QButtonGroup()
        self.registerField(FNSDKInstMOptArg, self, "mOptArgument")
        self.registerField(FNSDKInstMOptArgText, self, "mOptArgumentText")

        layout = QtGui.QVBoxLayout()

        # add all of the buttons
        for index, item in enumerate(self.__txt2opt):

            btn = QtGui.QRadioButton(item[0])

            layout.addWidget(btn)
            self.button_group.addButton(btn, index)

            if index == default_btn_index: # check the default button
                btn.setChecked(True)

        self.setLayout(layout)

    def getMOptArgument(self):
        '''Returns the argument of the -m option to be used with the SDK
        installer according to current selection'''
        return self.__txt2opt[self.button_group.checkedId()][1]

    def getMOptArgumentText(self):
        '''Returns the description of the -m option, with mnemonics removed'''
        return self.__txt2opt[self.button_group.checkedId()][0].replace('&', '')

    mOptArgument = QtCore.pyqtProperty(
        "QString", getMOptArgument,
        doc = "Argument to be passed to the -m option of the SDK installer")

    mOptArgumentText = QtCore.pyqtProperty(
        "QString", getMOptArgumentText,
        doc = "Description (UI text) of argument to be passed to the -m "
        "option of the SDK installer")

class VDSOPage(QtGui.QWizardPage):
    '''VDSO support page, should be shown only if system has unsupported
    VDSO.'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("VDSO support")
        self.setSubTitle("Current VDSO settings of this host are not "
                         "supported by %s." % (SB_NAME))

        info_str = ("To enable the installation of %s and %s this installer "
                    "will set VDSO kernel parameter to %s supported "
                    "value, which is 0. By default VDSO parameter will be "
                    "set for this session only, which means the setting "
                    "will persist until the next boot. If you would like the "
                    "installer to set VDSO parameter permanently please "
                    "check the checkbox below."
                    % (SB_NAME, PRODUCT_NAME, SB_NAME))

        info_label = QtGui.QLabel(info_str)
        info_label.setWordWrap(True)

        set_perm_vdso_checkbox = QtGui.QCheckBox(
            "Permanently set &VDSO kernel parameter to 0")

        txt = ("If checked the VDSO kernel parameter will be set to %s "
               "compatible value" % (SB_NAME))
        set_perm_vdso_checkbox.setToolTip(txt)
        set_perm_vdso_checkbox.setWhatsThis(txt)

        self.registerField(FNVDSOSetPerm, set_perm_vdso_checkbox)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(info_label)
        layout.addSpacing(40)
        layout.addWidget(set_perm_vdso_checkbox)
        self.setLayout(layout)

class NokiaBinsPage(QtGui.QWizardPage):
    '''Nokia Binaries page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("Nokia Binaries")
        self.setSubTitle("Nokia Binaries are essential for the complete "
                         "functionality of %s." % PRODUCT_NAME_SHORT)

        self.group_box = QtGui.QGroupBox(
            "&Install Nokia Binaries into %s targets" % SB_NAME)
        self.group_box.setFlat(True)

        eula_url = "http://tablets-dev.nokia.com/eula/index.php"

        info_str = ('Please visit this '
        '<a href="%s">webpage</a> to '
        'obtain Nokia Binaries sources.list entry. Then paste that entry into '
        'the edit box below in order for Nokia Binaries to be installed into '
        'targets by this installer' % (eula_url,))

        info_label = QtGui.QLabel(info_str)
        info_label.setWordWrap(True)
        info_label.setOpenExternalLinks(True)
        info_label.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse or
                                           QtCore.Qt.LinksAccessibleByKeyboard)

        txt = "Use link to visit %s to get sources.list entry" % eula_url
        info_label.setToolTip(txt)
        info_label.setWhatsThis(txt)

        self.repo_line_edit = QtGui.QLineEdit()

        group_box_layout = QtGui.QVBoxLayout()
        group_box_layout.addWidget(info_label)
        group_box_layout.addStretch()
        group_box_layout.addWidget(self.repo_line_edit)
        self.group_box.setLayout(group_box_layout)

        self.group_box.setCheckable(True)
        self.connect(self.group_box,
                     QtCore.SIGNAL("toggled(bool)"),
                     self.groupBoxToggled)

        # seems to be checked by default so toggle it
        self.group_box.setChecked(False) 
        self.group_box.setChecked(True)

        self.registerField(FNInstallNokiaBins, self.group_box, "checked",
                           "toggled(bool)")
        self.registerField(FNNokiaBinsRepo, self.repo_line_edit)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.group_box)
        self.setLayout(layout)

    def groupBoxToggled(self, on):
        '''Handler for group box's toggled signal. The goal is to focus on line
        edit when group box is enabled. The focus is removed from disabled line
        edit because if it has focus it steals all keyboard events, making
        keyboard unusable. Also, if line edit is disabled the focus is moved to
        the group box's checkbox, without this the focus would jump to the Next
        button.'''

        if on: # group box is enabled
            self.group_box.setFocusProxy(self.repo_line_edit)
        else:
            self.group_box.setFocusProxy(None)

        self.group_box.setFocus()

    def cleanupPage(self):
        '''Overridden, not to clean-up the repo text edit as the default
        implementation does.'''
        return

    def validatePage(self):
        '''Overrides the method of QWizardPage, verify valid input from the
        user'''

        # make sure the specified repo is close to valid
        if self.group_box.isChecked():
            repo = str(self.repo_line_edit.text())

            pat = r"deb http://repository.maemo.org/ fremantle/\w+ nokia-binaries$"
            m = re.match(pat, repo)

            if not m:
                QtGui.QMessageBox.critical(
                    self,
                    "Invalid repository!",
                    "Specified repository is invalid, please provide valid "\
                        "one!",
                    QtGui.QMessageBox.Ok)
                return False

        return True

class SummaryPage(QtGui.QWizardPage):
    '''Summary page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("Summary")

        self.setSubTitle("Please review installation configuration. At this "
                         "point, you can still go back and change settings.")

        # pages after this will have back button disabled
        self.setCommitPage(True)

        self.summary_label = QtGui.QLabel()
        self.summary_label.setWordWrap(True)
        self.summary_label.setAlignment(QtCore.Qt.AlignLeft |
                                        QtCore.Qt.AlignTop)

        # scroll area is used to add a scrolling capability in case the
        # text in the label becomes too long
        scroll_area = QtGui.QScrollArea(self)
        scroll_area.setWidgetResizable(True)
        scroll_area.setFrameStyle(QtGui.QFrame.NoFrame)
        scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

        # widget placed in scroll area
        sa_widget = QtGui.QWidget()

        # layout for the widget
        sa_widget_layout = QtGui.QVBoxLayout(sa_widget)
        sa_widget_layout.addWidget(self.summary_label)

        scroll_area.setWidget(sa_widget)

        # layout for the page
        layout = QtGui.QVBoxLayout()
        layout.addWidget(scroll_area)
        self.setLayout(layout)

    def initializePage(self):
        '''Overrides the method of QWizardPage, to initialize the page with
        summary text.'''

        self.wizard().setButtonText(QtGui.QWizard.CommitButton,
                                    "&Install %s" % (PRODUCT_NAME_SHORT))

        # create the summary text

        # scratchbox
        summary = "<b>%s %s: </b>%s<br><br>" % \
            (self.wizard().hostInfo.scratchbox_op_name, SB_NAME,
             bool_to_yesno(self.field(FNInstallScratchbox).toBool()))

        # SDK
        install_sdk = self.field(FNInstallSDK).toBool()

        summary += "<b>Install %s: </b>%s<br><br>" % \
            (PRODUCT_NAME, bool_to_yesno(install_sdk))

        # user
        summary += "<b>User to install SDK for: </b>%s<br><br>" % \
            (self.field(FNSelectedUsername).toString())

        # VDSO
        if self.wizard().hostInfo.has_unsupported_vdso:
            summary += "<b>Permanently set VDSO: </b>%s<br><br>" % \
                (bool_to_yesno(self.field(FNVDSOSetPerm).toBool()))

        # these are only available if SDK is installed
        if install_sdk:

            # targets
            target_x86_exist = self.field(FNTargetX86Exist).toBool()
            target_armel_exist = self.field(FNTargetArmelExist).toBool()

            if target_x86_exist or target_armel_exist: # they exist

                # remove targets
                remove_targets = self.field(FNRemoveTargets).toBool()

                summary += "<b>Remove targets: </b>%s<br><br>" % \
                    (bool_to_yesno(remove_targets))

                # target name prefix
                if not remove_targets:
                    summary += "<b>Target name prefix: </b>%s<br><br>" % \
                        (self.field(FNTargetPrefix).toString())
            
            # packages
            summary += "<b>Packages to install: </b>%s<br><br>" % \
                (self.field(FNSDKInstMOptArgText).toString())

            # Nokia Binaries
            install_nokia_bins = self.field(FNInstallNokiaBins).toBool()

            summary += "<b>Install Nokia Binaries: </b>%s<br><br>" % \
                (bool_to_yesno(install_nokia_bins))

            if install_nokia_bins:
                summary += "<b>Nokia Binaries repository: </b>%s<br><br>" % \
                    (self.field(FNNokiaBinsRepo).toString())

        # yes, I hate manual work
        dont_want_end = "<br><br>"
        if summary.endswith(dont_want_end):
            summary = summary[:-len(dont_want_end)]

        self.summary_label.setText(summary)

class ExecutorThread(QtCore.QThread):
    '''Thread that does the actual installation. This is not done in the GUI
    thread not to freeze the GUI.

    Because in GUI applications, the main thread (a.k.a. the GUI thread) is the
    only thread that is allowed to perform GUI-related operations, signals are
    sent from this thread to the main thread to give some visual feedback of
    completed operations.'''

    # thread exit status
    ExitStatusNotStarted = "status_not_started"
    ExitStatusOK         = "status_ok"
    ExitStatusAborted    = "status_aborted"
    ExitStatusError      = "status_error"

    def __init__(self, host_info, install_scratchbox, install_sdk,
                 sdk_inst_m_opt_arg, selected_username, targets_exist,
                 remove_targets, target_prefix, vdso_set_perm,
                 install_nokia_bins, nokia_bins_repo):
        '''Constructor'''
        QtCore.QThread.__init__(self)

        self.__host_info = host_info
        self.__install_scratchbox = install_scratchbox
        self.__install_sdk = install_sdk
        self.__sdk_inst_m_opt_arg = sdk_inst_m_opt_arg
        self.__selected_username = selected_username
        self.__targets_exist = targets_exist
        self.__remove_targets = remove_targets
        self.__target_prefix = target_prefix
        self.__vdso_set_perm = vdso_set_perm
        self.__install_nokia_bins = install_nokia_bins
        self.__nokia_bins_repo = nokia_bins_repo

        # for progress indication (a task can have many ops)
        self.__ops_total_num = self.__calcOpsTotalNum()
        self.__ops_done_num = 0 # number of done so far

        # abort installation after current task is completed
        self.__abort = False
        self.__is_running_last_task = False

        self.__exit_status = self.__class__.ExitStatusNotStarted

        # signals emitted when new installation operation started/ended (the
        # GUI thread uses these signals to update the progress bar and text on
        # a label)
        self.sig_op_started = QtCore.SIGNAL("opStarted")
        self.sig_op_ended = QtCore.SIGNAL("opEnded")

        LOG("Executor created: %s" % self)

    opsTotalNum = property(lambda self: self.__ops_total_num)
    exitStatus = property(lambda self: self.__exit_status)

    def abort(self):
        '''Called by the main thread to stop this thread after current
        installation operation completes. If the thread is running last task,
        the request to abort will be ignored, since the installation will end
        after that task anyway.

        It is possible to have the abort dialog shown in the beginning while
        the executor will proceed to run last task, in that case the user will
        have "Abort later" button visible even though last task is executed.'''

        if not self.isRunningLastTask():
            LOG("Executor accepted request to abort")
            self.__abort = True
        else:
            LOG("Executor ignoring abort request since running last task")

    def isAborting(self):
        '''Whether this thread is about to abort the installation.'''
        return self.__abort

    def isRunningLastTask(self):
        '''Whether this thread is executing last task.'''
        return self.__is_running_last_task

    def __str__(self):
        '''String representation method.'''
        return str(self.__dict__)

    def __calcOpsTotalNum(self):
        '''Returns number of total operations to complete the
        installation. Each task can have many operations.'''
        count = 0

        if self.__host_info.has_unsupported_vdso:
            count += 1

        if self.__install_scratchbox:
            count += 2

        if self.__install_sdk:
            count += 2

        if self.__install_nokia_bins:
            count += 2 # 1 for each target

        return count

    def __taskSetVdso(self):
        '''Sets VDSO kernel parameter to scratchbox supported value.

        Even if setting permanently, will first try to set for current boot
        session only, since the VDSO is known to crash some systems
        (e.g. Ubuntu Gutsy). If this is not done the system might become
        unbootable because of permanent VDSO settings.'''

        if self.__vdso_set_perm:
            self.__say("Setting VDSO permanently")

        else:
            self.__say("Setting VDSO for this session")

        # set it for this session only (will persist until the next boot)
        LOG("Attempting to set VDSO for this session")
        exec_cmd("sysctl -w vm.vdso_enabled=0")

        # now set it permanently
        if self.__vdso_set_perm:
            LOG("Permanently setting VDSO")

            file_append_lines("/etc/sysctl.conf",
                              ["",
                               "# This line was added by %s" % (MY_NAME),
                               "vm.vdso_enabled = 0"])
            exec_cmd("sysctl -p")

    def __taskInstallScratchbox(self):
        '''Downloads scratchbox installer, installs scratchbox, removes the
        installer'''

        self.__say("Downloading %s installer" % (SB_NAME))
        sb_installer_fn = download_file(SB_INSTALLER_URL)

        user_opt = "-u %s" % self.__selected_username

        # upgrade scratchbox
        if self.__host_info.has_scratchbox:

            # check if user already exist in scratchbox
            if os.path.isdir("%s/users/%s" % (SB_PATH,
                                              self.__selected_username)):
                LOG("User already exists in scratchbox, not using -u option")
                user_opt = ""


            self.__say("Upgrading %s" % (SB_NAME))
            exec_cmd(sb_installer_fn + " -f " + user_opt)

        # install scratchbox
        else:
            self.__say("Installing %s" % (SB_NAME))
            exec_cmd(sb_installer_fn + " "  + user_opt)

        LOG("Removing %s installer (%s)" % (SB_NAME, sb_installer_fn))
        os.remove(sb_installer_fn)

    def __taskInstallSdk(self):
        '''Downloads SDK installer, installs SDK, removes the installer'''

        self.__say("Downloading %s installer" % (PRODUCT_NAME_SHORT))
        sdk_installer_fn = download_file(SDK_INSTALLER_URL,
                                         self.__selected_username)

        # make the installer really non-interactive
        comment_line = "license\n"
        lines = open(sdk_installer_fn).readlines()

        for index, line in enumerate(lines):
            if line == comment_line:
                lines[index] = "# " + line
                LOG("Successfully patched the SDK installer")
                break
        else:
            raise Exception("SDK installer is strange, line %s not found!" %
                            comment_line)

        open(sdk_installer_fn, "w").writelines(lines)


        # do the installation thing
        cmd = "%s -d -m %s" % (sdk_installer_fn, self.__sdk_inst_m_opt_arg)

        if self.__targets_exist:
            if self.__remove_targets:
                cmd += " -y"

            else:
                cmd += " -n %s" % self.__target_prefix

        self.__say("Installing %s targets" % PRODUCT_NAME_SHORT)

        exec_cmd(cmd, self.__selected_username, True)

        LOG("Removing %s installer (%s)" % (PRODUCT_NAME_SHORT,
                                            sdk_installer_fn))
        os.remove(sdk_installer_fn)

    def __taskInstallNokiaBins(self):
        '''Installs Nokia Binaries into the targets'''
        # different target prefix was chosen
        if self.__targets_exist and not self.__remove_targets:
            armel_target = self.__target_prefix + TARGET_POSTFIX_ARMEL
            x86_target = self.__target_prefix + TARGET_POSTFIX_X86

        # default target names
        else:
            armel_target = TARGET_ARMEL
            x86_target = TARGET_X86

        for target in [armel_target, x86_target]:

            self.__say("Installing Nokia Binaries into target %s" % (target))

            # select target
            LOG("Selecting target %s in scratchbox" % target)
            cmd = "%s/tools/bin/sb-conf select %s" % (SB_PATH, target)
            exec_cmd(cmd, self.__selected_username, True)

            # add repo to sources.list
            sources_list_fn = (
                "%s/users/%s/targets/%s/etc/apt/sources.list" %
                (SB_PATH, self.__selected_username, target))

            LOG("Adding Nokia Binaries repo (%s) to sources.list of the "
                "target (%s)" % (self.__nokia_bins_repo, sources_list_fn))

            file_append_lines(
                sources_list_fn,
                ["",
                 "# Repository for Nokia Binaries, added by %s" % (MY_NAME),
                 self.__nokia_bins_repo])

            # install the binaries
            LOG("Starting the installation of Nokia Binaries")
            cmd = ("%s/login fakeroot apt-get update && %s/login "
                   "fakeroot apt-get install nokia-binaries --force-yes -y" %
                   (SB_PATH, SB_PATH))

            exec_cmd(cmd, self.__selected_username, True)

    def __getTasks(self):
        '''Returns list of installation tasks to be executed in order'''

        tasks = []

        if self.__host_info.has_unsupported_vdso:
            tasks.append(self.__taskSetVdso)

        if self.__install_scratchbox:
            tasks.append(self.__taskInstallScratchbox)

        if self.__install_sdk:
            tasks.append(self.__taskInstallSdk)
            
        if self.__install_nokia_bins:
            tasks.append(self.__taskInstallNokiaBins)

        LOG("Got tasks: %s" % tasks)
        return tasks

    def run(self):
        '''Runs the installation'''
        tasks = self.__getTasks()

        try:
            for task in tasks:

                if self.__abort:
                    LOG("Executor aborting")
                    self.__exit_status = self.__class__.ExitStatusAborted
                    break

                else:
                    LOG("Executor starting task %s" % (task))

                    if task == tasks[-1]:
                        self.__is_running_last_task = True
                        LOG("Started task is the last one")
                        
                    task()

            # all tasks successfully completed
            else:
                self.__exit_status = self.__class__.ExitStatusOK
                # for the last op
                self.emit(self.sig_op_ended, self.__ops_done_num)

        except: # CmdExecError, e:
            LOG.log_exc()
            self.__exit_status = self.__class__.ExitStatusError

        LOG("Executor set exit status to (%s)" % (self.__exit_status))

    def __say(self, msg):
        '''Communicates with the main thread about installation progress using
        signals'''

        self.emit(self.sig_op_ended, self.__ops_done_num) # previous op ended

        self.__ops_done_num += 1 # new op started

        assert self.__ops_done_num <= self.__ops_total_num, "Too many ops!"

        msg = "%s/%s %s" % (self.__ops_done_num, self.__ops_total_num, msg)

        LOG("S: %s" % msg)
        self.emit(self.sig_op_started, msg, self.__ops_done_num)

class ProgressPage(QtGui.QWizardPage):
    '''Progress page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        # whether the slider in the text widget has been scrolled to its
        # vertical scroll bars end, it will be scrolled only once at
        # initialization; when the slider is scrolled to the vertical end, the
        # newly appended text will cause automatic scrolling, so the new text
        # will always be visible
        self.scrolled_to_vend = False

        self.setTitle("Installing")
        self.setSubTitle("Please wait, installation in progress...")

        self.status_label = QtGui.QLabel("Installing...")
        self.status_label.setWordWrap(True)

        self.progress_bar = QtGui.QProgressBar()

        txt = ('Log file <a href="file://%s">%s</a>' %
               (LOG.fn_log, LOG.fn_log))

        self.logs_url_label = QtGui.QLabel(txt)
        self.logs_url_label.setWordWrap(True)
        self.logs_url_label.setOpenExternalLinks(True)
        self.logs_url_label.setTextInteractionFlags(
            QtCore.Qt.TextBrowserInteraction)

        # some systems have such bug that file can't be opened by clicking on
        # URL, so no hint on using the link, see
        # https://bugs.launchpad.net/ubuntu/+source/xdg-utils/+bug/362121

#         txt = "Use link to open log file"
#         logs_url_label.setToolTip(txt)
#         logs_url_label.setWhatsThis(txt)

        self.logs_text_edit = QtGui.QTextEdit()

        txt = "Logs from %s.  Use Ctrl+Wheel to zoom the text." % LOG.fn_log
        self.logs_text_edit.setToolTip(txt)
        self.logs_text_edit.setWhatsThis(txt)

        # set proper coloring of the text edit
        # must be done before writing any text, otherwise black text
        # will not be visible once you change background to black
        #
        # new in Qt 4.4: not used now, since some systems won't have it
        # self.logs_text_edit.setTextBackgroundColor(QtCore.Qt.black)
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Active, QtGui.QPalette.Base,
                         QtCore.Qt.black);
        palette.setColor(QtGui.QPalette.Inactive, QtGui.QPalette.Base,
                         QtCore.Qt.black);
        self.logs_text_edit.setPalette(palette);
        self.logs_text_edit.setTextColor(QtCore.Qt.green)

        # there must be some text in the editor, otherwise the color of text
        # written with cursor will be black, why? go figure...
        self.logs_text_edit.setPlainText("logs from %s\n" % LOG.fn_log)

        self.logs_text_edit.setReadOnly(True)

        # to look like a proper terminal use fixed width fonts
        self.logs_text_edit.setFont(QtGui.QFont("Monospace", 10))

        # cursor is used to append text to the end of the edit widget, there is
        # append method but it adds extra newline, and InsertPlainText method
        # inserts text at current cursor position, which can be changed by the
        # user just by clicking somewhere in the edit widget
        self.cursor = self.logs_text_edit.textCursor()
        self.cursor.movePosition(QtGui.QTextCursor.End)

        self.logs_button = QtGui.QPushButton("&Logs")
        txt = "Toggles visibility of the logs view"
        self.logs_button.setToolTip(txt)
        self.logs_button.setWhatsThis(txt)
        self.logs_button.setCheckable(True)
        self.connect(self.logs_button, QtCore.SIGNAL("toggled(bool)"),
                     self.logsButtonToggled)

        # takes the space of text edit when it is hidden
        self.spacer = QtGui.QSpacerItem(0, 0,
                                        QtGui.QSizePolicy.Minimum,
                                        QtGui.QSizePolicy.Expanding)

        # process that tails the log file
        self.tail_process = QtCore.QProcess(self)
        self.tail_process.start("tail -f " + LOG.fn_log)
        self.connect(self.tail_process,
                     QtCore.SIGNAL("readyReadStandardOutput()"),
                     self.tailProcessReadStdout)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.status_label)
        layout.addWidget(self.progress_bar)
        # spacer will be removed in button's toggled signal handler
        layout.addItem(self.spacer)
        layout.addWidget(self.logs_url_label)
        layout.addWidget(self.logs_text_edit)
        self.setLayout(layout)

        self.logs_button.setChecked(True) # will emit toggled signal

    def initializePage(self):
        '''Overrides the method of QWizardPage, to initialize the page with
        some default settings.'''

        QtGui.QApplication.setOverrideCursor(QtCore.Qt.BusyCursor)

        install_sdk = self.field(FNInstallSDK).toBool()
        target_x86_exist = self.field(FNTargetX86Exist).toBool()
        target_armel_exist = self.field(FNTargetArmelExist).toBool()
        targets_exist = target_x86_exist or target_armel_exist

        self.executor = ExecutorThread(
            self.wizard().hostInfo,
            self.field(FNInstallScratchbox).toBool(),
            install_sdk,
            str(self.field(FNSDKInstMOptArg).toString()),
            str(self.field(FNSelectedUsername).toString()),
            targets_exist,
            targets_exist and self.field(FNRemoveTargets).toBool(),
            str(self.field(FNTargetPrefix).toString()),
            self.wizard().hostInfo.has_unsupported_vdso and \
                self.field(FNVDSOSetPerm).toBool(),
            install_sdk and self.field(FNInstallNokiaBins).toBool(),
            str(self.field(FNNokiaBinsRepo).toString()))

        self.progress_bar.setRange(0, self.executor.opsTotalNum)
        self.progress_bar.setValue(0) # kinda redundant

        self.connect(self.executor, self.executor.sig_op_started,
                     self.executorOpStarted)

        self.connect(self.executor, self.executor.sig_op_ended,
                     self.executorOpEnded)

        self.connect(self.executor, QtCore.SIGNAL("finished()"),
                     self.executorFinished)

        self.executor.start()

        # have a custom button to show/hide logs
        self.wizard().setOption(QtGui.QWizard.HaveCustomButton1, True)
        self.wizard().setButton(QtGui.QWizard.CustomButton1, self.logs_button)

    def validatePage(self):
        '''Overrides the method of QWizardPage, to remove the custom
        button.'''
        self.wizard().setOption(QtGui.QWizard.HaveCustomButton1, False)

        # QWizard deletes the old button when setButton is used
        self.wizard().setButton(QtGui.QWizard.CustomButton1, None)

        return True

    def isBusy(self):
        '''Returns True if installation/removal is in progress'''
        return self.executor.isRunning()

    def executorOpStarted(self, msg, op_num): 
        '''Called when the thread has started executing an operation'''
        self.status_label.setText(msg)

    def executorOpEnded(self, op_num):
        '''Called when the thread has ended executing an operation'''
        self.progress_bar.setValue(op_num)

    def executorFinished(self):
        '''Called when executor thread has finished its jobs. This means
        installation or removal is complete at this point. So this routine
        reflects that to the UI'''

        self.setTitle("Installation process completed")

        # error
        if self.executor.exitStatus == ExecutorThread.ExitStatusError:
            self.setSubTitle("Installation was aborted by fatal error.")

            # show the failed op the label
            self.status_label.setText(
                "<font color=red><b>Failed: %s</font></b>" %
                self.status_label.text())

        # abort
        elif self.executor.exitStatus == ExecutorThread.ExitStatusAborted:
            self.setSubTitle("Installation was aborted by the user.")
            self.status_label.setText("Aborted")

        # ok
        elif self.executor.exitStatus == ExecutorThread.ExitStatusOK:
            self.setSubTitle("Installation completed successfully.")
            self.status_label.setText("Done")

        else:
            assert False, "Illegal thread exit status (%s)!" % \
                (self.executor.exitStatus)

        # enable the Next/Finish button
        self.emit(QtCore.SIGNAL("completeChanged()"))

        # Finish button will replace Next button if installation failed
        # setFinalPage works with Qt versions 4.5 and higher, see the bug:
        # http://www.qtsoftware.com/developer/task-tracker/index_html?method=entry&id=222140
        if self.isLastPage():
            if QtCore.QT_VERSION >= 0x040500:
                self.setFinalPage(True)

            else:
                finish_btn = self.wizard().button(QtGui.QWizard.FinishButton)
                finish_btn.setVisible(True)
                finish_btn.setEnabled(True)
                finish_btn.setDefault(True)
                
                next_btn = self.wizard().button(QtGui.QWizard.NextButton)
                next_btn.setVisible(False)

        # from this page on there is nothing to cancel, so disabled
        self.wizard().button(QtGui.QWizard.CancelButton).setEnabled(False)

        QtGui.QApplication.restoreOverrideCursor()

    def isLastPage(self):
        '''Returns True if this page is the last one to show (in which case
        Finish button will replace the Next button). This page will be the last
        if installation fails.'''
        return self.executor.exitStatus in \
            [ExecutorThread.ExitStatusAborted, ExecutorThread.ExitStatusError]

    def nextId(self):
        '''Overrides the method of QWizardPage, not to show last page in case
        installation fails.'''
        # installation failed
        if self.isLastPage():
            return -1

        # installation succeeded
        else:
            return QtGui.QWizardPage.nextId(self)

    def isComplete(self):
        '''Overrides the method of QWizardPage, to disable next button when
        installation/removal is in progress.'''
        if self.isBusy():
            return False
        else:
            return True

    def showAbortMsgBox(self):
        '''Shows abort message box. Returns tuple of buttons texts.'''

        msg_box = QtGui.QMessageBox(
            QtGui.QMessageBox.Question,
            "Really abort?",
            "Installation processes are still running!")

        # have not chosen to abort already in the past and not running last task
        have_abort_later_btn = not self.executor.isAborting() and \
            not self.executor.isRunningLastTask()

        abort_now_btn_txt = "Abort now"
        abort_later_btn_txt = "Abort later"

        abort_now_info_text = (
            "If you choose to '%s' all of the installation processes will be "
            "killed. <b>NOTE!</b> <i>This could potentially make your system "
            "unstable. So use it at your own risk.</i>" % abort_now_btn_txt)

        if have_abort_later_btn:
            abort_later_info_text = (
                "You can abort safely by choosing '%s', in which case "
                "currently running installation process will be allowed to "
                "complete its execution." % (abort_later_btn_txt))

        # don't have the abort later button
        else:
            if self.executor.isRunningLastTask():
                abort_later_info_text = (
                    "It appears that the last installation process is running. "
                    "Hence, installation should be over any minute now! It is "
                    "highly recommended to wait for the completion of "
                    "installation instead of aborting.")

            else:
                abort_later_info_text = (
                    "You have previously selected '%s', so installation will "
                    "be aborted after currently running installation process "
                    "completes its execution." % (abort_later_btn_txt))

        info_txt = (abort_later_info_text + "<br><br>" + abort_now_info_text)

        msg_box.setInformativeText(info_txt)

        cancel_btn = msg_box.addButton(QtGui.QMessageBox.Cancel)
        abort_now_btn = msg_box.addButton(abort_now_btn_txt,
                                          QtGui.QMessageBox.DestructiveRole)
        if have_abort_later_btn:
            abort_later_btn = msg_box.addButton(abort_later_btn_txt,
                                                QtGui.QMessageBox.AcceptRole)

        msg_box.setDefaultButton(cancel_btn)

        msg_box.exec_()

        clicked_btn = msg_box.clickedButton()
        LOG("Answer to abort dialog: %s" % clicked_btn.text())

        # msg_box & clicked_btn will not exist after this function returns
        return (clicked_btn.text(), abort_now_btn_txt, abort_later_btn_txt,
                cancel_btn.text())

    def onCancel(self):
        '''Called if user tries to close or cancel the wizard, shows the abort
        dialog and takes respective actions.'''
        (clicked_btn_txt, abort_now_btn_txt,
         abort_later_btn_txt, cancel_btn_txt) = self.showAbortMsgBox()

        # cannot abort if installation has already ended
        if clicked_btn_txt != cancel_btn_txt:
            if not self.isBusy():
                QtGui.QMessageBox.information(
                    self,
                    "Sorry!",
                    "Cannot '%s' since the installation has ended!" % \
                        (clicked_btn_txt),
                    QtGui.QMessageBox.Ok)
                return
        
        # abort later
        if clicked_btn_txt == abort_later_btn_txt:
            if self.executor.isRunningLastTask():
                QtGui.QMessageBox.information(
                    self,
                    "Sorry!",
                    "Cannot '%s' since the last installation process is "\
                        "running!" % abort_later_btn_txt, 
                    QtGui.QMessageBox.Ok)
            else:
                self.executor.abort()

        # abort now: kill self & children (running installers) too
        elif clicked_btn_txt == abort_now_btn_txt:
            LOG("Committing suicide...")
            os.killpg(os.getpid(), signal.SIGTERM)

    def showEvent(self, event):
        '''Overriden method of QWidget. Scrolls the slider of the vertical
        scroll bar of the text edit to the end.  This is done to enable
        automatic scrolling of the scrollbar upon addition of the new text.
        Done only once, when the page is shown for the first time.'''
        QtGui.QWizardPage.showEvent(self, event)

        if not self.scrolled_to_vend:
            self.logsTextEditScrollToVend()
            self.scrolled_to_vend = True

    def logsButtonToggled(self, checked):
        '''Slot for the toggled signal of the logs button. Hides/shows logs
        widgets.'''
        self.logs_text_edit.setVisible(checked)
        self.logs_url_label.setVisible(checked)

        if checked:
            # when text edit is visible spacer is removed, otherwise spacer
            # would visibly consume space if page is resized
            self.layout().removeItem(self.spacer)

        else:
            # when text edit is hidden the spacer is used to take its space to
            # prevent other widget from moving in the layout
            self.layout().insertItem(2, self.spacer)

    def logsTextEditScrollToVend(self):
        '''Scrolls vertical scroll bar of the text edit widget to the end'''
        vsb = self.logs_text_edit.verticalScrollBar()
        vsb.setValue(vsb.maximum())

    def logsTextEditRemoveLastLine(self):
        '''Removes last line from the text edit'''
        cursor = self.logs_text_edit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.select(QtGui.QTextCursor.BlockUnderCursor)
        cursor.deletePreviousChar()

    def splitStringWithMultiCR(self, txt):
        '''Splits a string containing multiple lines of text into list a of
        single-line strings. Character \r is assumed to mark the beginning of a
        line, whereas character \n is assumed to mark the end of a line. This
        routine is used to emulate terminal carriage return (\r) handling.

        returns a list of strings
        txt = a string containing multiple lines of text (including \n, \r etc)
        '''
        txt_list = []

        i_begin = 0 # index, where the next line should begin from

        for i in xrange(0, len(txt)):

            # \r is assumed to mark the beginning of a new line
            if txt[i] == '\r':

                # there could be \r just after line ending with \n or the whole
                # text could begin with \r: we don't want empty string in those
                # cases
                if i - i_begin > 0:
                    txt_list.append(txt[i_begin:i])
                    i_begin = i

            # \n is assumed to mark the end of a current line, next char after
            # newline will be the start of the next line
            elif txt[i] == '\n':
                txt_list.append(txt[i_begin:i + 1])
                i_begin = i + 1

        # if text does not end with \n, get the last line
        if i_begin < len(txt):
            txt_list.append(txt[i_begin:len(txt)])

        return txt_list

    def logsTextEditAppend(self, txt):
        '''Appends text into the text edit widget. If there are carriage return
        characters in the text does some processing in order to emulate
        terminal behavior.

        txt = a string containing multiple lines of text (including \n, \r etc)
        '''
        vsb = self.logs_text_edit.verticalScrollBar()

        at_bottom = vsb.value() == vsb.maximum()

        # \r\n is just newline (apt uses it while selecting/unpacking)
        txt = txt.replace("\r\n", "\n")

        # number of carriage return characters in the text
        cr_count = txt.count('\r')

        # text without carriage return is just inserted
        if cr_count == 0:
            self.cursor.insertText(txt)

        # text has only one carriage return in the beginning, previous line
        # must be removed before the text is inserted
        elif cr_count == 1 and txt.startswith('\r'):
            self.logsTextEditRemoveLastLine() 
            self.cursor.insertText(txt)

        # text has multiple carriage return characters
        else:
            txt_list = self.splitStringWithMultiCR(txt)
            for line in txt_list:
                if line.startswith('\r'):
                    self.logsTextEditRemoveLastLine()
                self.cursor.insertText(line)

        # automatically scroll, if slider was at the end of scrollbar
        if at_bottom:
            self.logsTextEditScrollToVend()

    def tailProcessReadStdout(self):
        '''A slot that is called when the tail process has some text on its
        stdout. Appends that text to the text edit.'''
        txt = str(self.tail_process.readAllStandardOutput())
        self.logsTextEditAppend(txt)

class ConclusionPage(QtGui.QWizardPage):
    '''ConclusionPage page'''

    def __init__(self):
        '''Constructor'''

        QtGui.QWizardPage.__init__(self)

        self.setTitle("Installation of %s completed" % PRODUCT_NAME)

        self.info_label = QtGui.QLabel()
        self.info_label.setWordWrap(True)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.info_label)
        self.setLayout(layout)

    def initializePage(self):
        '''Overrides the method of QWizardPage, to initialize the page with
        some dynamic text.'''
        self.info_label.setText(
            "Thank you for using %s!\n\n"
            "If this is the first time %s was configured for user %s, then "
            "to be able to login to %s, that user will have to either run "
            "'newgrp sbox' in the terminal, or logout and re-login into "
            "system." %
            (PRODUCT_NAME, SB_NAME, self.field(FNSelectedUsername).toString(),
             SB_NAME))

PageIdIntro          = 0
PageIdLicense        = 1
PageIdInstallOpts    = 2
PageIdVDSO           = 3
PageIdUsers          = 4
PageIdTargets        = 5
PageIdPkg            = 6
PageIdNokiaBins      = 7
PageIdSummary        = 8
PageIdProgress       = 9
PageIdConclusion     = 10

class InstallWizard(QtGui.QWizard):
    '''Installation wizard'''

    def __init__(self):
        '''Constructor'''

        # create own group so can kill (Abort Now) self & children during
        # installation: this is redundant in shells but needed if script is
        # launched from desktop e.g. by using kdesudo
        os.setpgrp()

        QtGui.QWizard.__init__(self)

        self.setWindowTitle(MY_NAME)

        self.__host_info = HostInfo()

        # self.setOption(QtGui.QWizard.NoBackButtonOnStartPage)
        # self.setOption(QtGui.QWizard.NoBackButtonOnLastPage)
        self.setOption(QtGui.QWizard.DisabledBackButtonOnLastPage)

        self.setPage(PageIdIntro, IntroPage())
        self.setPage(PageIdLicense, LicensePage())
        self.setPage(PageIdInstallOpts,
                     InstallOptsPage(self.__host_info.has_scratchbox,
                                     self.__host_info.scratchbox_op_name))
        if self.__host_info.has_unsupported_vdso:
            self.setPage(PageIdVDSO, VDSOPage())
        self.setPage(PageIdUsers, UsersPage())
        self.setPage(PageIdTargets, TargetsPage())
        self.setPage(PageIdPkg, PkgPage())
        self.setPage(PageIdNokiaBins, NokiaBinsPage())
        self.setPage(PageIdSummary, SummaryPage())
        self.setPage(PageIdProgress, ProgressPage())
        self.setPage(PageIdConclusion, ConclusionPage())

    hostInfo = property(lambda self: self.__host_info)

    def reject(self):
        '''Overridden method of QDialog to disable wizard closing when
        installation/removal is in progress. Handles closing wizard by:
        - pressing Esc button
        - clicking Cancel button
        - clicking X button on the title bar of the window
        - any shortcut that can be used to close a window
        - probably any other means used to close a window'''

        if self.currentId() == PageIdProgress and self.currentPage().isBusy():

            self.currentPage().onCancel()
            return

        # default behavior in other cases
        QtGui.QWizard.reject(self)

    def nextId(self):
        '''Returns ID of page to show when the user clicks the Next button.

        After the Users page if SDK not to be installed, returns the summary
        page. Targets page is not shown if targets don't exist for the selected
        user.
        '''
        if self.currentId() == PageIdUsers:

            install_sdk = self.field(FNInstallSDK).toBool()
            target_x86_exist = self.field(FNTargetX86Exist).toBool()
            target_armel_exist = self.field(FNTargetArmelExist).toBool()

            if not install_sdk:
                return PageIdSummary

            else: # sdk will be installed
                if not target_x86_exist and not target_armel_exist:
                    return PageIdTargets + 1 # next page
            
        # default behavior for other pages
        return QtGui.QWizard.nextId(self)

def run_checks():
    '''Performs some system checks, raises exception if some check fails'''

    # this script must run with root privileges: installation of SDK only could
    # be done without root priveleges, but the SDK installer requires VDSO to
    # be set to scratchbox compatible value, which can only be done as root
    if os.geteuid() != 0:
        raise Exception("Root access is needed! Please run this application "
                        "with root privileges!")

    # only 32 bit systems are supported
    sup_machines = ["i386", "i686"]
    machine = os.uname()[4]

    if machine not in sup_machines:
        raise Exception("Operating system's machine or word size '%s' is not "
                        "supported. Only 32 bit systems are supported %s." %
                        (machine, sup_machines))

def main():
    '''Main.'''
    app = QtGui.QApplication([])

    parse_options()

    try:
        run_checks()
    except Exception, e:
        QtGui.QMessageBox.critical(None, MY_NAME, str(e), QtGui.QMessageBox.Ok)
        sys.exit(1)

    global LOG
    LOG = Logger() # must have root acces to re-write previous log file

    wizard = InstallWizard()
    wizard.show()

    return app.exec_()

if __name__ == "__main__":
    main()
