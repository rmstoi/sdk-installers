#!/usr/bin/env python

'''Installer/uninstaller for the Maemo SDK'''

#       (C) Copyright 2007-2009 by Nokia Corporation. All rights reserved.
#
#       Contact: Ruslan Mstoi <ruslan.mstoi@nokia.com>
#
#       Permission is hereby granted, free of charge, to any person
#       obtaining a copy of this software and associated documentation
#       files (the "Software"), to deal in the Software without
#       restriction, including without limitation the rights to use,
#       copy, modify, merge, publish, distribute, sublicense, and/or sell
#       copies of the Software, and to permit persons to whom the
#       Software is furnished to do so, subject to the following
#       conditions:
#
#       The above copyright notice and this permission notice shall be
#       included in all copies or substantial portions of the Software.
#
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#       OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#       NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#       HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#       WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#       FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#       OTHER DEALINGS IN THE SOFTWARE.


# $Id: maemo-sdk-installer.py 1759 2009-05-05 14:20:58Z rmstoi $

import subprocess
import sys
import os
import time
import shutil
import pwd
import textwrap
import urllib2
import ConfigParser
import traceback
import tarfile

from optparse import OptionParser

# keys into the systems dictionary
SUS_REPO = 'repo'
SUS_HAS_ECLIPSE_IDE = 'ide' # IDE does not work on all systems (java portability issues)

# supported systems and related information
SUPPORTED_SYSTEMS = {

    "Ubuntu" : {

        "gutsy" : {
            SUS_REPO : "deb http://maemo-sdk.garage.maemo.org/download/host ubuntu-gutsy free",
            SUS_HAS_ECLIPSE_IDE : False
        },

        "hardy": {
            SUS_REPO : "deb http://maemo-sdk.garage.maemo.org/download/host ubuntu-hardy free",
            SUS_HAS_ECLIPSE_IDE : False
        },

        "intrepid": {
            SUS_REPO: "deb http://maemo-sdk.garage.maemo.org/download/host ubuntu-intrepid free",
            SUS_HAS_ECLIPSE_IDE : True
        },

        "jaunty": {
            SUS_REPO: "deb http://maemo-sdk.garage.maemo.org/download/host ubuntu-jaunty free",
            SUS_HAS_ECLIPSE_IDE : True
        }
    },

    "Debian" : {

        "etch" : {
            SUS_REPO : "deb http://maemo-sdk.garage.maemo.org/download/host debian-etch free",
            SUS_HAS_ECLIPSE_IDE : False
        },

        "lenny" : { 
            SUS_REPO : "deb http://maemo-sdk.garage.maemo.org/download/host debian-lenny free",
            SUS_HAS_ECLIPSE_IDE : True
        }
    }
}

# rootstraps that will be selected for installation by default
DEFAULT_ROOTSTRAPS = ["diablo4.1.2_i386", "diablo4.1.2_armel"]

# names shown to the user (what is installed/removed and installer's name)
PRODUCT_NAME = 'Maemo SDK+'
PRODUCT_NAME_SHORT = 'SDK+'
MY_NAME = '%s Installer' % PRODUCT_NAME

# SDK time & space consumption: these are very rough
INST_CONS_TIME = '20 minutes'
INST_CONS_SPACE = "2GB"

# command line options and arguments
OPTIONS = None
ARGS = None
OPT_PARSER = None

# messenger, initialized later on, after root access is verified
# because log file can exist already with root permissions
MSGR = None
IMPORTER = None # also initialized later

# True if additional software is available for installation
HAVE_ADDSW = False

# license text for Nokia Binaries, NOT for this script
EUSA_TEXT = \
'''
IMPORTANT: READ CAREFULLY BEFORE INSTALLING, DOWNLOADING, OR USING THE SOFTWARE

This Software includes some software copyrighted by Nokia Corporation
or third parties in binary form (Licensed Software) and some Open Source
Software in binary and source code form.

The Licensed Software is licensed to you under the Nokia Software
Development Kit Agreement [see below] and distributed to you only in
binary form.

The Open Source Software is licensed and distributed under the GNU General
Public License (GPL), the GNU lesser General Public License (LGPL,
aka. The GNU Library General Public License) and/or other copyright
licenses, permissions, notices or disclaimers containing obligation or
permission to provide the source code of such software with the binary /
executable form delivery of the said software. Any source code of such
software that is not part of this delivery is made available to you in
accordance with the referred license terms and conditions on this page
http://www.maemo.org. Alternatively, Nokia offers to provide any such
source code to you on CD-ROM for a charge covering the cost of performing
such distribution, such as the cost of media, shipping and handling,
upon written request to Nokia at:

Source code Requests
Multimedia
Nokia Corporation
P.O.Box 407
FIN00045 Nokia Group
Finland.

This offer is valid for a period of three (3) years.

The exact license terms of GPL, LGPL and said certain other licenses,
as well as the required copyright and other notices, permissions and
acknowledgements are reproduced in and delivered to you as part of the
referred source code.


NOKIA SOFTWARE DEVELOPMENT KIT AGREEMENT

This Software Development Kit Agreement ("Agreement") is between You
(either an individual or an entity), the Licensee, and Nokia Corporation
("Nokia"). The Agreement authorizes You to use the Software specified
in Clause 1.4 below, which may be stored on a CD-ROM, sent to You by
electronic mail, or downloaded from Nokia's Web pages or Servers or from
other sources under the terms and conditions set forth below. This is
an agreement on licensee rights and not an agreement for sale. Nokia
continues to own the copies of the Software, which is the subject of
this Agreement, and any other copies that You are authorized to make
pursuant to this Agreement.

Read this Agreement carefully before installing, downloading, or using
the Software.  By clicking on the "I Accept" button, or by typing "I
Accept" while installing, downloading, and/or using the Software, You
agree to the terms and conditions of this Agreement. If You do not agree
to all of the terms and conditions of this Agreement, promptly click
the "Decline" or "I Do Not Accept" button, or do not type "I Accept"
and cancel the installation or downloading, or destroy or return the
Software and accompanying documentation to Nokia. YOU AGREE THAT YOUR
USE OF THE SOFTWARE ACKNOWLEDGES THAT YOU HAVE READ THIS AGREEMENT,
UNDERSTAND IT, AND AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS.

1. Definitions

1.1 "Affiliates" of a party shall mean an entity (i) which is directly or
indirectly controlling such party; (ii) which is under the same direct or
indirect ownership or control as such party; or (iii) which is directly
or indirectly owned or controlled by such party. For these purposes an
entity shall be treated as being controlled by another if that other
entity has fifty percent (50 %) or more of the votes in such entity,
is able to direct its affairs and/or to control the composition of its
board of directors or equivalent body.

1.2 "Documentation" shall mean any documents, drawings, models, layouts
and/or any other IPR that are documented in any media and that have been
provided by Nokia or any of its Affiliates to Licensee.

1.3 "IPR" shall mean any and all artifacts and knowledge related to the
Licensed Software, to the extent either generated in connection with the
evaluation or use of the Licensed Software, incorporated and/or used in
the design or function of any result of such evaluation or use and/or
developed and/or discovered during such evaluation or use, including
without limitation the components, prototypes, documents, designs and
computer software, as well as ideas, inventions, patents (including
utility models), and designs (whether or not capable of registration),
chip topography rights and other like protection, copyrights (including
software copyright of source code, object code, executable code etc.),
trademarks, know how, sui generis rights to data or databases and any
other form of statutory protection of any kind and applications for any
of the foregoing respectively as well as any trade secrets.

1.4 "Licensed Software" As used in this Agreement, the term "Software"
means, collectively: (i) the software copyrighted by Nokia Corporation
or third parties (excluding Open Source Software), which is delivered to
Licensee as part of the Nokia Software Development Kit for the MAEMO-
Platform (ii) all the contents of the disk(s), CD-ROM(s), electronic
mail and its file attachments, or other media with which this Agreement
is provided, including the object code form of the software delivered
via a CD-ROM, electronic mail, or Web page (iii) digital images, stock
photographs, clip art, or other artistic works ("Stock Files") (iv)
related explanatory written materials and any other possible documentation
related thereto ("Documentation"); (v) fonts, and (vi) upgrades, modified
versions, updates, additions, and copies of the Software (collectively
"Updates"), if any, licensed under this Agreement.

1.5 "Pre-Existing IPR" shall mean any IPR that has not been either
generated in connection with the evaluation or use of the Licensed
Software, incorporated and/or used in the design or function of any
result of such evaluation or use and/or developed and/or discovered
during such evaluation or use.

1.6 "Third Party Software" shall mean software created by neither of
the Parties.

2. PERIOD AND SCOPE OF THE LICENSEE's PERMITTED USE

2.1 The Licensee shall be entitled to exploit the license granted to it by
Nokia under and in accordance with this Agreement solely for the purpose
of porting and developing software for the MAEMO-Platform (the "Purpose").

2.2 Nokia may choose to deliver, at its sole discretion, updates for
the Licensed Software. In addition Nokia may choose, at its exclusive
discretion, to make available, either as downloadable modules from a
dedicated website or as electronic deliveries via e-mail, to the Licensee
the subsequent updates of the Licensed Software as well as any subsequent
directly related minor upgrades, if any. As a prerequisite to the making
available of any subsequent upgrades and/or updates, Nokia may require,
at its sole discretion, the Licensee to accept additional terms and
conditions in connection with the delivery and prior to any use of any
such subsequent upgrades and/or updates made available to the Licensee
by Nokia or its Affiliates. The Parties may also separately agree on
the licensing of any future releases of the Licensed Software, if any,
to be part of the Agreement by concluding a necessary amendment to the
Agreement and by agreeing on the respective terms and conditions.

3. GRANT OF RIGHTS

3.1 Subject to the terms and conditions of this Agreement, Nokia
grants to Licensee, and Licensee hereby accepts, a non-transferable,
non-sublicenseable, non-exclusive, limited license to install and use
Licensed Software on the local hard disk(s) or other permanent storage
media, copy, run and utilize the Licensed Software in object code form
solely for the Purpose. In addition, Licensee may make one extra copy of
the Licensed Software as an archival backup copy. Any other copies made by
the Licensee of the Licensed Software are in violation of the Agreement.

3.2 Notwithstanding the generality of subsection 3.1 above and insofar
as the Licensee is provided with components of the Licensed Software in
source code form ("Source Code Components"), Nokia grants to Licensee
and Licensee hereby accepts, subject to the terms and conditions of
this Agreement, a non-transferable, non-sublicenseable, non-exclusive,
limited license to compile such Source Code Components, without any
modification whatsoever, with Licensee Application(s) solely for the
Purpose and prepare copies of the Source Code Components necessary for
such compilation. For the sake of clarity, the Licensee rights under
this subsection 3.2 shall be subject to the limitations set forth in
Clause 4 below.

3.3 Parts of the Licensed Software may be supplied by third parties
and may be subject to separate license terms. The Licensee shall accept
any such license terms applicable to Third Party Software that may be
introduced as part of the installation of the Licensed Software.

4. LIMITATIONS OF LICENSE AND LOAN

4.1 Licensee shall have no right to disclose, sell, market, commercialise,
sub-license, rent, lease, re-license or otherwise transfer to any other
party, whatsoever, the Licensed Software or any part thereof, or use the
Licensed Software for any purpose or in any manner that is not expressly
permitted in this Agreement.

4.2 Licensee shall not modify or develop any derivative works
of the Licensed Software unless specifically authorized by this
Agreement. Licensee may not reverse engineer, decompile, or disassemble
any part of the Licensed Software, except and only to the extent that
such activity is expressly permitted by applicable law notwithstanding
this limitation.

4.3 The Licensee understands that the rights and licenses granted under
this Agreement with respect to the Licensed Software are granted subject
to third party intellectual property rights and as such may not be enough
to use or develop the Licensed Software or any application created with
the Licensed Software, and therefore the Licensee and any users may
need to obtain separate licenses for necessary technologies in order to
implement the necessary solutions or use the Licensed Software.

4.4 Licensee undertakes to have written personal agreements between
Licensee and its employees (if any) having access to the Licensed Software
or any Software created using Licensed Software or any related information
for the purpose of ensuring compliance with the terms and conditions of
this Agreement.

4.5 Licensee undertakes not to disclose, license or allow any use of
software created using Licensed Software, outside the permitted Purpose,
without express prior written consent of Nokia.

4.6 Misuse of the Licensed Software or data generated by the Licensed
Software is strictly prohibited by Nokia, and may violate U.S. and other
laws. Licensee is solely responsible for any misuse of the Licensed
Software under this Agreement and Licensee agrees to indemnify Nokia
and its Affiliates for any liability or damage related in any way to
Licensee's use of the Licensed Software in violation of the same. Licensee
is also responsible for using the Licensed Software in accordance with
the limitations of this Agreement. Licensee will indemnify and hold Nokia
and its Affiliates harmless for any claims arising out of the use of the
Licensed Software or data generated by the Licensed Software on devices
belonging to the Licensee or any of Licensee's customers or any third
party that has been provided access to the Licensed Software or data
generated by the Licensed Software, except to the extent those claims
arise out of the Nokia's breach of this Agreement.

4.7 There are no implied licenses or other implied rights granted
under this Agreement, and all IPR and rights, save for those licenses
expressly granted to the Licensee hereunder, shall remain with and vest
in Nokia. Nokia shall retain at all times a right to license, market,
develop, modify and customize the Licensed Software. The non-exclusive
rights granted to Licensee under this Agreement should not be construed
as a limitation on any activities of Nokia or any of its Affiliates
in the use of the Licensed Software and the marketing or licensing of
the Licensed Software, and Nokia retains all of its rights, title and
interest in the IPR, the Licensed Software and in any derivative works
thereof. Except for the rights and licenses granted to Licensee herein,
Licensee shall not take any action inconsistent with such title and
ownership.

5. FEEDBACK

Licensee may provide Nokia comments, suggestions, opinions and the like as
feedback on the characteristic, performance, functionality and use of the
Licensed Software (the "Feedback"). Feedback includes without limitation
materials as well as ideas or know how (whether presented orally, in
written form or otherwise). Any Feedback submitted by the Licensee or
any of its employees or subcontractors shall, after submission to Nokia,
be owned by Nokia. Nokia shall receive, upon submission of Feedback,
a worldwide, perpetual, irrevocable, fully paid-up, royalty-free,
exclusive, transferable, sub-licenseable license to use, copy, modify,
make available, incorporate in any products or services of Nokia,
sublicense, distribute and otherwise make use of the Feedback and to
create derivative works thereof to the extent and in such manner as
Nokia deems fit at its exclusive discretion. For the sake of clarity,
the license granted to Nokia above in this Clause 5 shall include rights
and licenses to any Pre-existing IPR of Licensee deemed necessary by
Nokia to exercise its rights relating to the Feedback.

6. EXCLUSION OF WARRANTIES 

6.1 LICENSEE ACKNOWLEDGES THAT THE LICENSED SOFTWARE IS PROVIDED "AS
IS" AND NOKIA, ITS AFFILIATES AND/OR ITS LICENSORS DO NOT MAKE ANY
REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR
PURPOSE OR THAT THE LICENSED SOFTWARE WILL NOT INFRINGE ANY THIRD PARTY
PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.

6.2 THERE IS NO WARRANTY BY NOKIA, ITS AFFILIATES AND/OR ITS LICENSORS OR
BY ANY OTHER PARTY THAT THE LICENSED SOFTWARE WILL MEET THE LICENSEE'S
REQUIREMENTS OR THAT THE OPERATION OF THE LICENSE SOFTWARE WILL BE
UNINTERRUPTED OR ERROR-FREE. LICENSEE ASSUMES ALL RESPONSIBILITY AND RISK
FOR THE SELECTION OF THE LICENSED SOFTWARE TO ACHIEVE LICENSEE'S INTENDED
RESULTS AND FOR THE INSTALLATION, USE, AND RESULTS OBTAINED FROM IT.

6.3 THIS AGREEMENT CREATES NO OBLIGATIONS ON THE PART OF NOKIA OTHER
THAN AS SPECIFICALLY SET FORTH HEREIN.

7. LIMITATION OF LIABILITY

7.1 IN NO EVENT SHALL NOKIA, ITS EMPLOYEES, LICENSORS, AFFILIATES
OR AGENTS BE LIABLE FOR ANY LOST PROFITS OR COSTS OF PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES, PROPERTY DAMAGE, LOSS OF PROFITS,
INTERRUPTION OF BUSINESS OR FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
ECONOMIC, COVER, PUNITIVE, OR CONSEQUENTIAL DAMAGES, HOWEVER CAUSED,
AND WHETHER ARISING UNDER CONTRACT, TORT, NEGLIGENCE, OR OTHER THEORY OF
LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THE LICENSED
SOFTWARE, EVEN IF THE OTHER PARTY IS ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.

7.2 THE TOTAL AGGREGATE LIABILITY OF NOKIA SHALL, ON ALL OCCASIONS
AND FOR ANY DAMAGES OF WHATSOEVER NATURE ARISING IN WHATSOEVER MANNER,
SHALL BE LIMITED TO U.S. $10.

8. TECHNICAL SUPPORT

8.1 In connection with the delivery of the Licensed Software under this
Agreement, Nokia may make technical support relating to the Licensed
Software available to the Licensee e.g. by providing access to a dedicated
support website, but Nokia has no obligation whatsoever to furnish the
Licensee with any technical support unless separately agreed in writing
between Licensee and Nokia.

8.2 In the event Nokia provides the Licensee with any technical support
under subsection 8.1 above without a separate written agreement,
such provision shall not (i) obligate Nokia to continue to provide any
technical support, (ii) enable the Licensee to make any claims regarding
the quality, timeliness or other characteristics of the technical support
being provided, (iii) create any obligation for Nokia to enter into a
written support agreement with the Licensee.

8.3 Any technical support that may be provided by Nokia shall be provided
without warranties of any kind, unless separately agreed in writing
between the Parties.

9. TERM AND TERMINATION

9.1 The Agreement shall become effective upon Acceptance by Licensee
as set forth above. Any terms and conditions that by their nature
or otherwise reasonably should survive a termination or an expiry of
the Agreement shall be deemed to survive. Such terms and conditions
include but are not limited to the obligations set forth in Clauses 1,
5-8, 10-12.

9.2 Nokia will have the right to terminate the Agreement for any reason
at any time, in its sole discretion, with a prior written notice of thirty
(30) days.

9.3 Upon expiry or termination of the Agreement Licensee shall return
all information received and materials made available by Nokia under
this Agreement, including but not limited to Licensed Software, to
Nokia immediately.

10. EXPORT CONTROL 

The Licensed Software, including technical data, includes cryptographic
software subject to export controls under the U.S. Export Administration
Regulations ("EAR") and may be subject to import or export controls in
other countries. The EAR prohibits the use of the Software and technical
data by a Government End User, as defined hereafter, without a license
from the U.S. government. A Government End User is defined in Part
772 of the EAR as "any foreign central, regional, or local government
department, agency, or other entity performing governmental functions;
including governmental research institutions, governmental corporations,
or their separate business units (as defined in part 772 of the EAR)
which are engaged in the manufacture or distribution of items or
services controlled on the Wassenaar Munitions List, and international
governmental organizations. This term does not include: utilities
(telecommunications companies and Internet service providers; banks
and financial institutions; transportation; broadcast or entertainment;
educational organizations; civil health and medical organizations; retail
or wholesale firms; and manufacturing or industrial entities not engaged
in the manufacture or distribution of items or services controlled on the
Wassenaar Munitions List.)". Licensee agrees to strictly comply with all
applicable import and export regulations and acknowledge that Licensee has
the responsibility to obtain licenses to export, re-export, transfer, or
import the Licensed Software. Licensee further represent that Licensee is
not a Government End User as defined above, and Licensee will not transfer
the Licensed Software to any Government End User without a license.

11. NOTICES

All notices and return of the Licensed Software and Documentation should
be delivered to:

Head of Legal Department
NOKIA CORPORATION
MULTIMEDIA BUSINESS GROUP
P.O. Box 100
FIN-00045 NOKIA GROUP
FINLAND

12. MISCELLANOUS

12.1 Licensee and Nokia are independent Parties. Nothing in this
the Agreement shall be construed to make either Party an agent,
employee, franchisee, joint venture or legal representative of the other
Party. Neither Party will have nor represent itself to have any authority
to bind the other Party or act on its behalf. Nothing in these General
Conditions or the Agreement shall constitute or imply any promise or
intention by Nokia to enter into any other business arrangement with
Licensee.

12.2 If any provision of the Agreement or its Appendices is held invalid,
all other provisions shall remain valid unless such validity would
frustrate the purpose of the Agreement, this Agreement and its Appendices
shall be enforced to the full extent allowable under applicable law.

12.3 Licensee shall bear all costs that it may incur in connection with
the use of the Licensed Software.

12.4 This Agreement is governed by the laws of Finland excluding its
choice of law provisions. All disputes arising from or relating to
the Agreement shall be settled by a single arbitrator appointed by the
Central Chamber of Commerce of Finland. The arbitration procedure shall
take place in Helsinki, Finland in the English language.

'''

class FriendlyException(Exception):
    '''User friendly exception, friendly because the traceback is not
       shown to the user.

       Raised in subroutines/methods to indicate failure. For this type of
       exception the installer will normally print the message to stdout and
       log the traceback.'''
    pass

class Messenger(object):
    '''A communication channel between the installation routines and the UI
    classes. Additionally handles all logging.'''

    def __init__(self):
        '''Constructor'''

        # list of strings describing non-critical errors that have occurred
        # during execution, there can be multiple non-critical errors
        self.__errors = []

        # string describing one (and only one) fatal error that has occurred,
        # if fatal error occurs installer will give up
        self.__fatal = ''

        # last thing said
        self.__last_say_msg = ''

        self.__override_say = None
        self.__override_ask = None

        script_name = os.path.basename(sys.argv[0])

        # file name of the log file: script name, with extenstion replaced
        self.fn_log = "/tmp/%s.log" % script_name[:script_name.rfind('.')]

        self.fo_log = None

        try:
            self.fo_log = open(self.fn_log, 'w')
        except:
            print ("Could not open log file %s!" % (self.fn_log,))
            raise

        self.log("Running version: %s" % OPT_PARSER.get_version())

    def __del__(self):
        '''Destructor'''
        if self.fo_log:
            self.fo_log.close()

    def log(self, msg):
        """Writes a log message."""
        self.fo_log.write("V: %s\n" % msg)
        self.fo_log.flush()

    def log_exc(self):
        '''Writes current exception information into the log file'''
        fmt = '-' * 5 + " %s " + '-' * 5

        self.log(fmt % "Begin logging exception")
        traceback.print_exc(file = self.fo_log)
        self.log(fmt % "End logging exception")

    def say(self, msg):
        '''Writes a message to the UI, by default to the command line.
        Of course it is still possible to use print to write text to stdout,
        but if text is to be written to either GUI and command line UI, then
        this method should be used'''
        if self.__override_say:
            self.__override_say(msg)
        else:
            sys.stdout.write(msg + "\n")

        self.log("S: " + msg)
        self.__last_say_msg = msg

    def set_override_say(self, new_say):
        '''Overrides the default UI say method. GUI can use this in order
        to redirect messages to the GUI
        new_say  = function that takes a string a parameter'''
        self.__override_say = new_say
        
    def remove_override_say(self):
        '''Removes the override for UI say method'''
        assert self.__override_say, "Override is not set!"
        self.__override_say = None

    def ask(self, title, question, choices, default_choice):
        '''Asks a question in the UI, by default the command line.'''
        self.log("A: '%s' '%s' %s %s" % (title, question, choices,
                                         default_choice))

        if self.__override_ask:
            answer = self.__override_ask(title, question, choices,
                                         default_choice)
        else:
            answer = ask(title, question, choices, default_choice)

        self.log("A: Answer = %s" % answer)
        return answer

    def set_override_ask(self, new_ask):
        '''Overrides the default UI ask method. GUI can use this in order
        to redirect questions to the GUI
        new_ask  = new ask function'''
        self.__override_ask = new_ask
        
    def remove_override_ask(self):
        '''Removes the override for UI ask method'''
        assert self.__override_ask, "Override is not set!"
        self.__override_ask = None

    def get_fatal(self):
        '''fatal getter'''
        return self.__fatal

    def set_fatal(self):
        '''fatal setter'''
        assert not self.__fatal, "Fatal is already set!"

        self.__fatal = self.__last_say_msg
        self.log("Set fatal to: %s" % self.__fatal)

    def get_errors(self):
        '''Returns sequence object containing errors'''
        return self.__errors

    def add_error(self):
        '''Adds an error to the collection of errors'''
        err_msg = self.__last_say_msg
        self.__errors.append(err_msg)
        self.say("Failed: %s" % err_msg)

CmdNameInstall = 'install'
CmdNameRemove  = 'remove'

class Command(object):
    '''A command.'''

    def __init__(self):
        '''Constructor.'''
        self.name = None
        self.func = None
        
        self.check_can_run()

    def run(self, *args, **kwds):
        '''Runs the command.
        Arguments = passed to the function that actually executes the
                    command'''
        try:
            self.func(*args, **kwds)
        except (CmdExecError, FriendlyException), e:
            MSGR.set_fatal()
            MSGR.log(str(e))
            MSGR.log_exc()

    def check_can_run(self):
        '''Tests if nothing prevents the command from running, if yes, raises
        an exception.'''
        pass

    def is_install(self):
        '''Returns True if command is install, False otherwise'''
        return self.name == CmdNameInstall

    def is_remove(self):
        '''Returns True if command is remove, False otherwise'''
        return self.name == CmdNameRemove

class InstallCommand(Command):
    '''Install command.'''

    def __init__(self):
        '''Constructor.'''
        Command.__init__(self)

        self.name = CmdNameInstall
        self.func = sdk_install

    def run(self, *args, **kwds):
        '''Runs the command.
        Arguments = check from sdk_install'''
        Command.run(self, *args, **kwds)

    def check_can_run(self):
        '''Tests if nothing prevents the command from running, if yes, raises
        an exception.'''

        msg = ("Previous installation detected: %s. Please use " +
               CmdNameRemove  +  " command of this script to uninstall "
               "previous installation, then try again!")

        # check if maemo-sdk command exist
        try:
            subprocess.Popen(["maemo-sdk"], stdout = subprocess.PIPE,
                             stderr = subprocess.PIPE)

        except OSError:
            # does not exist, so fine
            pass
        else:
            # exists
            raise FriendlyException(msg % ("maemo-sdk command exists"))

        # check if /opt/maemo exists and is non-empty: the dirs are listed in
        # RemoveSelections, if all needed get them from there
        dir = "/opt/maemo"
        if os.path.exists(dir) and os.listdir(dir):
            raise FriendlyException(msg % (dir + " is non-empty"))

class RemoveCommand(Command):
    '''Remove command.'''

    def __init__(self):
        '''Constructor.'''
        Command.__init__(self)

        self.name = CmdNameRemove
        self.func = sdk_remove

    def run(self, *args, **kwds):
        '''Runs the command.
        Arguments = check from sdk_remove'''
        Command.run(self, *args, **kwds)

def create_command(args):
    '''Parses the command arguments and creates a command object based on the
    arguments. On success returns command object. On faulure, raises friendly
    exception.

    args = command line arguments (the specified command)'''

    if len(args) != 1:
        raise FriendlyException("One command must be specified!")

    cmds = { CmdNameInstall : InstallCommand, CmdNameRemove : RemoveCommand }
    
    try:
        cmd_class = cmds[args[0].lower()]
    except KeyError:
        raise FriendlyException("Invalid command %s" % (args[0],))

    return cmd_class()

class DynamicImporter(object):
    '''Manages dynamically imported modules. Most of these are modules that are
    not distributed with python. Keeps the imported modules in a dictionary, so
    they could be re-used. If module cannot be imported will attempt to install
    respective package using package manager.

    Package manager is not used to check if the respective package is already
    installed, because the user can install 3rd party modules not using package
    manager.'''

    def __init__(self, pkg_mgr):
        '''Constructor.'''
        self.__pkg_mgr = pkg_mgr

        self.__modules = {} # imported modules

        # dictionary of packages that contain modules, if module cannot be
        # imported attempt will be made to install respective package
        self.__packages = {
            'PyQt4.QtGui' : pkg_mgr.pkg_name_pyqt4,
            'PyQt4.QtCore' : pkg_mgr.pkg_name_pyqt4,
            'pexpect' : pkg_mgr.pkg_name_pexpect
            }

    def __import_module(self, module_name):
        '''Imports a module. If module cannot be imported will attempt to
        install respective package. Raises FriendlyException if import fails.

        module_name = name of the module, check help on __import__ for
                      more info'''

        def do_import():
            '''Wrapper around repetitve command'''
            self.__modules[module_name] = __import__(module_name, globals(),
                                                     locals(), [''])

        # attempt to import
        try:
            do_import()

        # the module is missing, try to install package
        except ImportError:

            package_name = self.__packages.get(module_name)
            
            if not package_name:
                raise FriendlyException(
                    "Failed to import %s and don't know how to install it! "
                    "You might have to install those modules yourself." %
                    module_name)

            # try to install the package
            if is_yes("Package %s is needed but not installed, install?"
                      % package_name):

                self.__pkg_mgr.pkg_install(package_name)
                do_import()

            # user chose not to install the package, raise exception, the
            # caller might have a workaround for this
            else:
                raise FriendlyException("Not installing missing package %s." %
                                        package_name)

    def get_module(self, module_name):
        '''Returns requested module if module already imported. If module is
        not imported yet, will import it and then return it. Might raise
        exceptions as mentioned in __import_module.

        module_name = see __import_module'''

        if module_name not in self.__modules:
            MSGR.log("Requested module %s not imported, will try to import" %
                     module_name)
            self.__import_module(module_name)

        else:
            MSGR.log("Requested module %s already imported" % module_name)

        return self.__modules[module_name]

    def __getitem__(self, module_name):
        '''Just a convenience method'''
        return self.get_module(module_name)

class SysInfo(object):
    '''Some information about operating system'''

    def __init__(self):
        '''Constructor'''
        self.platform = None  # OS type (e.g. linux2)
        self.machine = None   # OS machine type (e.g. i686)
        self.distro = None    # Linux distro ID (e.g. Ubuntu)
        self.codename = None  # OS release codename (e.g. hardy)

        self.__get_info()

    def __get_info(self):
        '''Gets the OS info
        Raises Exception on failure.'''

        self.platform = sys.platform

        # linux specific stuff (e.g. windows does not supports uname)
        if self.platform.startswith("linux"):

            self.machine = os.uname()[4]

            # get distribution & release information 
            # in Ubuntu platform.dist returns wrong information
            # (see https://bugs.launchpad.net/python/+bug/196526), thus
            # lsb_release is used
            try:
                self.distro = subprocess.Popen(["lsb_release", "-si"],
                                               stderr = subprocess.STDOUT,
                                               stdout = subprocess.PIPE
                                               ).communicate()[0].strip()

                self.codename = subprocess.Popen(["lsb_release", "-sc"],
                                                 stderr = subprocess.STDOUT,
                                                 stdout = subprocess.PIPE
                                                 ).communicate()[0].strip()
            except OSError, e:
                raise FriendlyException(
                    "Exception caught while getting OS info: %s."
                    "Failed to find distribution information: "
                    "is lsb_release installed?" % e)

        # non-linux system, to be implemented when needed
        else:
            pass

    def __str__(self):
        '''String representation method.'''
        return str(self.__dict__)

class UsersSelections(object):
    def __init__(self, all_users, all_rootstraps):
        '''Constructor.
        all_users = dictionary of all users
        all_rootstraps = dictionary of all rootstrap from the index file'''

        # names of users for whom to install the rootstraps
        self.users = set()

        # names of rootstraps to install for the selected users
        self.rootstraps = set()

        self.__add_default_users(all_users)
        self.__add_default_rootstraps(all_rootstraps)

    def __add_default_users(self, all_users):
        '''Adds the default user to the selected users'''
        ent =  get_default_user()

        if ent:
            # add only users that are in the all_users
            if ent.pw_name in all_users:
                MSGR.log("Adding default user: %s" % ent)
                self.users.add(ent.pw_name)

            else:
                MSGR.log("Not adding '%s' as default user: not in list of "
                         "users!" % ent.pw_name)

    def __add_default_rootstraps(self, all_rootstraps):
        '''Adds rootstraps to be installed by default to the set of rootstraps
        all_rootstraps = rootstraps from the index file'''
        MSGR.log("Found default rootstraps: %s" % DEFAULT_ROOTSTRAPS)

        for r in DEFAULT_ROOTSTRAPS:

            # add only rootstraps that are in the index file
            if r in all_rootstraps:
                self.rootstraps.add(r)

            else:
                MSGR.log("Rootstrap %s is in defaults, but not available "
                         "according to the the index file. Omitting..." % r)

        MSGR.log("Selected default rootstraps: %s" % self.rootstraps)

    def __str__(self):
        '''String representation method.'''
        return str(self.__dict__)

    def get_summary_text(self, newline = '\n', bold_begin = '', bold_end = ''):
        '''Return a string with the selections summary.
        newline = newline character to be used in the string
        bold_begin = beginning tag of the bold text
        bold_end = ending tag of the bold text'''
        pass

class InstallSelections(UsersSelections):
    '''User's selections in the install UI.'''

    def __init__(self, all_users, all_rootstraps):
        '''Constructor.
        all_users = dictionary of all users
        all_rootstraps = dictionary of all rootstrap from the index file'''

        UsersSelections.__init__(self, all_users, all_rootstraps)

        self.eusa_accepted = False # if True Nokia Binaries should be installed

        # it depends on maemo-sdk thus, it is removed automatically when sdk
        # package is removed
        self.install_eclipse_ide = False

    def get_summary_text(self, newline = '\n', bold_begin = '', bold_end = ''):
        '''See docstring of the same method in the parent class'''
        # users summary
        text = "%s%s%s%s" % (bold_begin, si.summary_txt_users, bold_end, newline)
        text += seq_to_str(self.users, newline, newline)

        # these are only printed if any users are chosen
        if self.users:
            text += newline

            # rootstraps summary
            text += "%s%s%s%s" % (bold_begin, si.summary_txt_roostraps, bold_end, newline)
            text += seq_to_str(self.rootstraps, newline, newline)

            # Nokia binaries are only installed if rootstraps are
            if self.rootstraps:
                text += "%s%s%s%s %s%s" % (newline,
                                           bold_begin,
                                           si.summary_txt_eusa,
                                           bold_end,
                                           bool_to_yesno(self.eusa_accepted),
                                           newline)

        if HAVE_ADDSW:
            # eclipse IDE summary
            text += "%s%s%s%s: %s%s" % (newline,
                                        bold_begin,
                                        si.addsw_txt_eclipse_ide,
                                        bold_end,
                                        bool_to_yesno(self.install_eclipse_ide),
                                        newline)

        return text

class RemoveSelections(UsersSelections):
    '''User's selections in the remove UI.'''

    def __init__(self, all_users, all_rootstraps):
        '''Constructor.
        all_users = dictionary of all users
        all_rootstraps = set to None, right now rootstraps are not used during
        removal'''

        UsersSelections.__init__(self, all_users, all_rootstraps)

        # list of SDK directories in system
        self.system_dirs = ["/opt/maemo/"]

        # list of SDK directories in user's home
        self.user_dirs = [".maemo-sdk", ".scratchbox2", "sb2_logs"]

        self.rm_system_dirs = False # remove system dirs
        self.rm_user_dirs = False   # remove dirs in users home
        self.make_backups = False

        # directory where backups are made
        self.backups_dir = "/opt/maemo-sdk-backups/"

    def get_remove_dirs(self, only_existent = False):
        '''Returns a list of directories to be removed, based on user's
        selections.

        only_existent = If True, will make sure that directories in the
                        returned list actually exist. The UI shows all
                        directories, even non-existing ones, while remove
                        routines need only existing directories'''

        remove_dirs = []

        def add_dir(dir):
            '''adds a directory into the list'''
            if only_existent:
                if os.path.exists(dir):
                    remove_dirs.append(dir)
            else:
                remove_dirs.append(dir)

        # dirs in users home
        if self.rm_user_dirs and self.users:
            for user in sorted(self.users):
                home_dir = os.path.expanduser("~" + user)
                for dir in self.user_dirs:
                    add_dir(os.path.join(home_dir, dir))

        # dirs in system
        if self.rm_system_dirs:
            for dir in self.system_dirs:
                add_dir(dir)

        MSGR.log("Got remove dirs: %s" % remove_dirs)

        return remove_dirs

    def have_remove_dirs(self):
        '''Returns True if according to selections any directories will get
        removed'''
        return self.rm_system_dirs or (self.rm_user_dirs and self.users)

    def have_remove_dirs_no_backups(self):
        '''Returns True if according to selections any directories will get
        removed and backups will not be made'''
        return self.have_remove_dirs() and not self.make_backups

    def get_summary_text(self, newline = '\n', bold_begin = '', bold_end = ''):
        '''See docstring of the same method in the parent class'''

        def wrap(text):
            '''Does text wrapping for command line UI and nothing for GUI,
            since GUI handles wrapping itself'''
            if newline == '\n':
                return get_wrapped(text)
            else:
                return text

        # system dirs
        text = "%s%s%s: %s%s" % (bold_begin,
                                 sr.rm_settings_txt_system_dirs,
                                 bold_end,
                                 bool_to_yesno(self.rm_system_dirs),
                                 newline)
        text += newline

        # users dirs
        text += "%s%s%s: %s%s" % (bold_begin,
                                  sr.rm_settings_txt_user_dirs,
                                  bold_end,
                                  bool_to_yesno(self.rm_user_dirs),
                                  newline)

        # users from whose homes to remove files
        if self.rm_user_dirs:
            text += newline

            text += "%s%s%s%s" % (bold_begin, sr.summary_txt_users, bold_end,
                                  newline)
            text += seq_to_str(self.users, newline, newline)

        # backups
        if self.rm_user_dirs or self.rm_system_dirs:
            text += newline
            text += "%s%s%s: %s%s" % (bold_begin,
                                      sr.rm_settings_txt_make_backups,
                                      bold_end,
                                      bool_to_yesno(self.make_backups),
                                      newline)

        rm_dirs = self.get_remove_dirs()

        # print the dirs if there are any
        if rm_dirs:
            text += newline
            text += wrap(
                "Due to your selections, the following directories and their "
                "contents will be permanently removed: ")

            text += newline
            text += newline
            text += seq_to_str(rm_dirs, newline, newline, False)

            if not self.make_backups:
                text += newline
                text += wrap(
                    "%sWARNING!%s These directories will be compeletely wiped "
                    "out and backups will not be made! If you have any data "
                    "in these directories you should choose to make backups!"
                    % (bold_begin, bold_end))
                text += newline

        return text

class PkgManager(object):
    '''System independent interface to the package manager. This is an abstract
    base class'''

    def __init__(self, sys_info):
        '''Constructor'''
        self.sys_info = sys_info

        # these are set in child classes
        self.src_dir = None  # repository sources directory
        self.src_file = None # name of the repository source file for maemo-sdk
        self.repo = None     # maemo-sdk repository
        self.pkg_name_sdk = None   # name of the maemo-sdk installation package
        self.pkg_name_pyqt4 = None # name of the PyQt4 package
        self.pkg_name_pexpect = None # name of the pexpect package
        self.pkg_name_eclipse_ide = None # name of the SDK Eclipse-based IDE package

        # look up the repo
        try:
            self.repo = SUPPORTED_SYSTEMS[self.sys_info.distro][self.sys_info.codename][SUS_REPO]
        except KeyError:
            raise FriendlyException("No repository available for this system!")

    def pkg_install(self, name, fatal = True):
        '''Installs a package
        name = name of the package to install
        fatal = True if the installation process should be executed as a
                fatal command'''

        MSGR.say("Installing package %s" % name)
        self.check_can_pkg_install()

    def pkg_remove(self, name, fatal = True):
        '''Removes a package
        name = name of the package to remove
        fatal = True if the removal process should be executed as a
                fatal command'''

        MSGR.say("Removing package %s" % name)
        self.check_can_pkg_remove()

    def check_can_pkg_install(self):
        '''Tests if packages can be installed, if not raises an exception'''
        pass

    def check_can_pkg_remove(self):
        '''Tests if packages can be removed, if not raises an exception'''
        pass

    def repo_install(self):
        '''Adds maemo-sdk repository to the list of package manager
        repository sources'''
        MSGR.say("Installing repository file %s" % self.src_file)

        if os.path.isdir(self.src_dir):

            f = open(self.src_file, "w")
            try:
                f.write("# This file was automatically generated by the %s\n"
                        % MY_NAME)
                f.write("# It contains repository for the %s\n" % PRODUCT_NAME)
                f.write("%s\n" % self.repo)
            finally:
                f.close()
        else:
            print "WARNING: There is no %s on this system!" % self.src_dir
            print "You will have to set up %s repository yourself (%s)"\
                % (PRODUCT_NAME, self.repo)

    def repo_remove(self):
        '''Removes maemo-sdk repositroy from the list of package manager
        repository sources'''
        MSGR.say("Removing repository file %s" % self.src_file)

        try:
            os.remove(self.src_file)
        except OSError, e:
            MSGR.log("Repository source file does not exist (%s)" % e)

    def sdk_install(self):
        '''Installs all SDK related stuff'''
        self.repo_install()
        self.pkg_install(self.pkg_name_sdk)

    def sdk_remove(self):
        '''Removes all SDK related stuff'''
        self.pkg_remove(self.pkg_name_sdk)
        self.repo_remove()

    def _exec_cmd(self, cmd, fatal):
        '''Executes a command.
        cmd = command to execute
        fatal = if True then command will be executed as critical'''
        if fatal:
            exec_cmd_fatal(cmd)
        else:
            exec_cmd_error(cmd)

class AptPkgManager(PkgManager):
    '''Interface to the Debian apt package manager'''

    def __init__(self, sys_info):
        '''Constructor. Throws KeyError if apt repository is not available.'''
        PkgManager.__init__(self, sys_info)

        self.src_dir = "/etc/apt/sources.list.d"
        self.src_file = os.path.join(self.src_dir, "maemo-sdk.list")

        self.pkg_name_sdk = "maemo-sdk"
        self.pkg_name_pyqt4 = "python-qt4"
        self.pkg_name_pexpect = "python-pexpect"
        self.pkg_name_eclipse_ide = "maemo-sdk-ide"

    def check_can_pkg_install(self):

        # loop while check for cache lock fails
        while exec_cmd("apt-get check -qq"):

            answer = MSGR.ask("",
                              "It appears apt cache is locked, please close "
                              "any applications using apt, then retry!",
                              [AskChoiceRetry, AskChoiceAbort],
                              AskChoiceRetry)

            if answer == AskChoiceAbort:
                raise FriendlyException("Could not use apt because of lock!")

    def check_can_pkg_remove(self):
        self.check_can_pkg_install()

    def pkg_install(self, name, fatal = True):
        PkgManager.pkg_install(self, name)

        self._exec_cmd("apt-get update -qq", fatal)
        self._exec_cmd("apt-get install %s --force-yes -y" % name, fatal)

    def pkg_remove(self, name, fatal = True):
        PkgManager.pkg_remove(self, name)

        self._exec_cmd("apt-get remove %s -y" % name, fatal)

        # remove useless dependencies, this will silently fail on systems that
        # don't support 'autoremove' (e.g. Debian Etch)
        exec_cmd("apt-get autoremove -y")

    def repo_install(self):
        PkgManager.repo_install(self)

    def repo_remove(self):
        PkgManager.repo_remove(self)

def pkg_manager(sys_info):
    '''System dependent package manager factory function'''

    if sys_info.distro in ["Ubuntu", "Debian"]:
        return AptPkgManager(sys_info)

    raise FriendlyException("No package manager available for this system!")

def exec_cmd(command, pwd_ent = None):
    '''Executes a command and returns the result
    pwd_ent = Password database entry, if set, the command will be executed
              with the credentials (UID & GID) of that user.'''

    if pwd_ent:
        MSGR.log("Executing as user %s: %s" % (pwd_ent.pw_name, command))
    else:
        MSGR.log("Executing: %s" % (command,))

    # if root privileges not needed, specify function to remove root privileges
    if pwd_ent:
        child_preexec_fn = lambda: set_guid(pwd_ent)
    else:
        child_preexec_fn = None

    p = subprocess.Popen("exec " + command,
                         stdout = MSGR.fo_log,
                         stderr = subprocess.STDOUT,
                         preexec_fn = child_preexec_fn,
                         shell = True)
    p.wait()

    return p.returncode

class CmdExecError(Exception):
    '''Exception raised when execution of a command fails.'''
    pass

def exec_cmd_fatal(command, pwd_ent = None):
    '''Executes a critical command and raises exception if command execution
    fails. Critical commands are the ones that must succeed in order for the
    installer to go on.

    pwd_ent = passed to exec_cmd'''
    returncode = exec_cmd(command, pwd_ent)

    if returncode:
        raise CmdExecError("Giving up, because failed to: %s" % command)

def exec_cmd_error(command, pwd_ent = None):
    '''Executes a command and stores an error if command execution fails. This
    function should be used to execute commands that are not critical, i.e. if
    they fail the installer can continue.

    pwd_ent = passed to exec_cmd'''
    returncode = exec_cmd(command, pwd_ent)

    if returncode:
        MSGR.add_error()

    returncode

def seq_to_str(seq, sep = '\n', last_sep = '\n', sort = True):
    '''Convers a sequence into a string. Returns the new string. If there are
    no items in the sequence then None is returned.
    sep = character to separate sequence items in the string
    last_sep  = character to be used after the last item
    sort = whether to sort the sequence before printing'''

    if not seq:
        return "None" + sep

    else:

        text_str = ''

        if sort:
            fseq = sorted(seq)
        else:
            fseq = seq

        for i in fseq:
            if i == fseq[-1]:
                nl = last_sep
            else:
                nl = sep

            text_str += i + nl

    return text_str

def bool_to_yesno(bool_value):
    '''Return yes if Boolean value is true, no if it is false'''
    if bool_value:
        return 'Yes'
    else:
        return 'No'

def print_wrapped(text):
    '''Prints wrapped text'''
    print get_wrapped(text)

def get_wrapped(text):
    '''Retrurns wrapped copy of text'''
    return textwrap.fill(text, width = 70)

def get_default_user():
    '''Returns password file entry for the user that invoked sudo or su to run
    this script. Returns None if couldn't get such entry.'''
    pwd_ent = None

    # try to get the user that ran sudo
    username = os.getenv('SUDO_USER')

    # or user that ran su
    if not username:
        username = os.getenv('USERNAME')

    if username:

        # installing SDK into root's home is not allowed, also the list of
        # users does not show system users
        if username == 'root':
            MSGR.log("Omitting default user 'root'")
            return None

        try:
            pwd_ent = pwd.getpwnam(username)
        except KeyError:
            pwd_ent = None

    return pwd_ent

# some choices used in multiple functions
AskChoiceYes = "y"
AskChoiceNo = "n"
AskChoiceAbort = "abort"
AskChoiceRetry = "retry"

def ask(title, question, choices, default_choice = None,
        have_choices_in_question = True):
    '''Command line version of ask.
    title = the title
    question = question to be asked (prompt)
    choices = exclusive choices the user can use as an answer
    default_choice = If set, choice to be selected when just return is pressed
    have_choices_in_question = self explanatory

    returns the selected choice'''

    assert choices, "No choices specified"
    assert not default_choice or \
        default_choice in choices, "Default choice not in choices"

    str_choices = "(%s)" % (seq_to_str(choices, "/", "", False))

    # show the default choice
    if default_choice:
        str_choices = str_choices.replace(default_choice,
                                          "[%s]" % default_choice)

    if have_choices_in_question:
        prompt = get_wrapped(question + " " + str_choices)
    else:
        prompt = get_wrapped(question)

    while True: # loop until got answer
        print

        if title:
            print title

        answer = raw_input(prompt + ' ')

        # user just pressed enter, return default answer
        if default_choice and answer == '':
            return default_choice

        if answer in choices:
            return answer

def is_yes(question, default_answer = True):
    '''Ask the user for yes/no answer and returns True if user answered yes
    False if no.
    default_answer = In case user just presses return, if this argument is
                     True, yes will be the default answer, otherwise no will
                     be the default answer'''

    if default_answer:
        default_choice = AskChoiceYes
    else:
        default_choice = AskChoiceNo

    answer = ask(None, question, [AskChoiceYes, AskChoiceNo], default_choice)

    if answer == AskChoiceYes:
        return True
    elif answer == AskChoiceNo:
        return False

def show_eusa():
    '''Shows Nokia EUSA and returns True if the user acceps it, False if the
    user does not accept it.'''

    p1 = subprocess.Popen(["echo", EUSA_TEXT], stdout = subprocess.PIPE)

    # less is more, maybe not as much compatible though
    p2 = subprocess.Popen(["less"], stdin = p1.stdout)
    p2.wait()

    # check if the user acceps the license
    answer = ''
    while answer == '':
        print "\nDo you accept all the terms of the preceding License Agreement?"
        print "Please reply with 'I accept' if you do."
        answer = raw_input("> ").lower()

    if answer == "i accept":
        print "Thank you."
        return True

    return False

def scan_seq(desc_intro, desc_name, all_items, selected_items):
    '''Shows list of all_items to the user and writes the selections into the
    selected_items set.
    desc_intro = introductory string for the list
    desc_name = what an item is called (e.g. rootstrap)
    all_items  = list of all items, the user will be selecting from this list
    selected_items = set of items selected by user, can have default values
    '''

    str_continue = 'c'
    str_all = 'a'
    str_none = 'n'

    while True:

        # print introduction
        print "\n"
        print_wrapped(desc_intro)
        print
        print_wrapped("Here is a list of %ss on your system, selected items "
                      "are marked with an asterisk before the index:"
                      % desc_name)
        print

        # print list
        for index, item in enumerate(all_items):
            if item in selected_items: # mark selected items
                print "*",
            else:
                print " ",
            print index, item,
            print

        # user chose to modify the list
        if is_yes("Would you like to change the selections in the list of %ss?"
                  % desc_name,
                  not len(selected_items)):# if any items: default answer is no

            print

            # read the new selections
            answer = ''
            while answer == '':  # loop until something entered
                print_wrapped("Enter space separated list of integer indices, "
                              "%s to select all, %s to unselect all, "
                              "or %s to continue: " % \
                                  (str_all, str_none, str_continue))
                answer = raw_input("> ").lower()

            # continue (i.e. quit this list menu)
            if answer == str_continue:
                break

            # select all items
            elif answer == str_all:
                selected_items.update(all_items)

            # clear all items
            elif answer == str_none:
                selected_items.clear()

            # user entered something
            else:
                indices = answer.split()
                cleared = False

                # loop through entered indices
                for ch_index in indices:

                    # convert character to int
                    try:
                        index = int(ch_index)
                    except ValueError:
                        print "Ignoring %s: not an integer index!" % ch_index
                        continue

                    # index within valid range
                    if 0 <= index < len(all_items):

                        # the first time adding, clear the previous entries
                        if not cleared:
                            selected_items.clear()
                            cleared = True

                        selected_items.add(all_items[index])

                    # index out of bounds
                    else:
                        print "Ignoring %s: out of bounds!" % index
                        continue

        # user chose not to modify list
        else:
            break

class si:
    '''Strings for installation'''

    name_noun = "installation"
    name_verb_simple_present = "install"
    name_verb_present_continuous = "installing"
    name_verb_simple_past = "installed"

    intro_title = 'Welcome to the %s installation wizard' % PRODUCT_NAME
    intro_subtitle = ('This application will guide you through installation of the '
    '%s. Installation will take about %s and %s of your disk '
    'space.' % (PRODUCT_NAME, INST_CONS_TIME, INST_CONS_SPACE))

    users_title = 'Users'
    users_subtitle = ('Please select users to install rootstraps for.')

    rootstraps_title = 'Rootstraps'
    rootstraps_subtitle = ('Please select rootstraps to install for the selected users.')

    license_title = 'EUSA for Nokia Binaries'
    license_subtitle = ('To install Nokia Binaries into the rootstraps you '
'will have to read and accept the terms of Nokia EUSA (End User Software '
'Agreement).')

    addsw_title = 'Additional software packages'
    addsw_subtitle = ('Please select software packages to be installed in addition to the %s' % (PRODUCT_NAME))
    addsw_txt_eclipse_ide = 'Install Eclipse-based IDE'

    # text with accelerators for GUI
    addsw_txt_eclipse_ide_acc = addsw_txt_eclipse_ide.replace("Eclipse", "&Eclipse")

    summary_title = 'Summary'
    summary_subtitle = 'Please review installation configuration. At this point, you can still go back and change settings'

    summary_txt_users = "Users into whose home directories rootstraps will be installed:"
    summary_txt_roostraps = "Rootstraps that will be installed:"
    summary_txt_eusa = "Install Nokia Binaries:"

    progress_title = 'Installing'
    progress_subtitle = 'Please wait, installation in progress...'
    progress_subtitle_done = "Done installing"

    conclusion_title = 'Installation of %s completed' % PRODUCT_NAME
    conclusion_subtitle = 'Thank you for using %s!' %PRODUCT_NAME

class sr:
    '''Strings for removal'''

    name_noun = "removal"
    name_verb_simple_present = "remove"
    name_verb_present_continuous = "removing"
    name_verb_simple_past = "removed"

    intro_title = 'Welcome to the %s removal wizard' % PRODUCT_NAME
    intro_subtitle = ('This application will help you remove %s '
'from your system. As a result, about %s of your disk space will be '
'freed.' % (PRODUCT_NAME, INST_CONS_SPACE))

    users_title = 'Users'
    users_subtitle = ('Please review the list of users '
'from whose home directories to remove %s files and rootstraps.'
% PRODUCT_NAME_SHORT)

    rootstraps_title = 'None'
    rootstraps_subtitle = 'None'

    license_title = 'None'
    license_subtitle = 'None'

    # remove settings page
    rm_settings_title = 'Removal settings'
    rm_settings_subtitle = 'Please select parts of %s to remove' % (PRODUCT_NAME_SHORT)
    rm_settings_txt_system_dirs = 'Remove %s files from system directories' % (PRODUCT_NAME_SHORT)
    rm_settings_txt_user_dirs = 'Remove %s files from selected users home' % (PRODUCT_NAME_SHORT)
    rm_settings_txt_make_backups = 'Make a backup copy of files to be removed'

    # text with accelerators for GUI
    rm_settings_txt_system_dirs_acc = rm_settings_txt_system_dirs.replace("system", "&system")
    rm_settings_txt_user_dirs_acc = rm_settings_txt_user_dirs.replace("users", "&users")
    rm_settings_txt_make_backups_acc = rm_settings_txt_make_backups.replace("Make", "&Make")

    summary_title = 'Summary'
    summary_subtitle = 'Please review removal configuration. At this point, you can still go back and change settings'
    summary_txt_users = "Users from whose home directories to remove %s files and rootstraps:" % (PRODUCT_NAME_SHORT)
    summary_txt_no_backups_verify = "Are you sure? Those directories will be removed and backups will not be made!"

    progress_title = 'Removing'
    progress_subtitle = 'Please wait while removing...'
    progress_subtitle_done = 'Done removing'

    conclusion_title = 'Removal of %s completed' % PRODUCT_NAME
    conclusion_subtitle = 'Thank you for using %s!' % PRODUCT_NAME

class sc:
    '''Common strings'''
    fatal_title = "Execution was aborted by fatal error during stage:"
    errors_title = "Non-critical errors occurred during these stages:"

class UI(object):
    '''Base user interface class'''

    def __init__(self):
        '''Constructor.'''

    def run_cmd(self, cmd, pkg_mgr, all_users, all_rootstraps):
        '''Reads configuration from the user and then runs specified
        command in the UI. The children should override it to provide
        actual functionality'''

class CmdLineUI(UI):
    '''The command line user interface class.'''

    def __init__(self):
        '''Constructor.'''

    def run_cmd(self, cmd, pkg_mgr, all_users, all_rootstraps):
        '''See docstring of the same method of the parent class'''

        # look up the correct data
        cmd_data = {
            CmdNameInstall : [self.__get_install_selections,
                              si,
                              InstallSelections],

            CmdNameRemove : [self.__get_remove_selections,
                             sr,
                             RemoveSelections]
            }

        get_selections = cmd_data[cmd.name][0]
        s = cmd_data[cmd.name][1]
        users_selections = cmd_data[cmd.name][2](all_users, all_rootstraps)

        # show into and return if the user does not want to continue
        if not self.__show_intro(s):
            return

        # loop until got user accepted selections
        while True:
            users_selections = get_selections(users_selections,
                                              all_users,
                                              all_rootstraps)

            if self.__show_summary(cmd, s, users_selections):
                break # summary accepted, run the command

        print
        print
        cmd.run(users_selections, pkg_mgr, all_users, all_rootstraps)

        # print error info
        if MSGR.get_fatal():
            print
            print sc.fatal_title
            print MSGR.get_fatal()
            print

        if MSGR.get_errors():
            print
            print sc.errors_title
            
            for err in MSGR.get_errors():
                print err

            print

    def __show_intro(self, s):
        # print intro
        print "\n%s!\n" % (s.intro_title)
        print_wrapped(s.intro_subtitle)

        return is_yes("Would you like to continue?")

    def __get_install_selections(self, install_selections, all_users,
                                 all_rootstraps):
        '''Command line user interface for the install command.
        Returns selections.
        install_selections = selections that will be used as default
        all_users = dictionary of all users
        all_rootstraps = dictionary of all rootstrap'''

        # read the users
        scan_seq(si.users_subtitle,
                 'user',
                 sorted(all_users), # convert dictionary into an indexable list
                 install_selections.users)

        # TODO: should print rootstrap sizes?
        # if any users selected then ask the rootstraps
        if install_selections.users:
            scan_seq(si.rootstraps_subtitle,
                     'rootstrap',
                     sorted(all_rootstraps),
                     install_selections.rootstraps)

            print "\n"
            print_wrapped(si.license_subtitle)
            if is_yes("Would you like to install Nokia Binaries into the "
                      "rootstraps?"):
                install_selections.eusa_accepted = show_eusa()

        if HAVE_ADDSW:
            # ask about Eclipse IDE package
            install_selections.install_eclipse_ide = \
                is_yes(si.addsw_txt_eclipse_ide + '?', False)

        print "\n"

        return install_selections

    def __get_remove_selections(self, remove_selections, all_users,
                                all_rootstraps):
        '''Command line user interface for the remove command.
        Returns selections.
        remove_selections = selections that will be used as default
        all_users = dictionary of all users
        all_rootstraps = dictionary of all rootstrap'''

        print

        # system dirs
        txt = "%s (%s)?" % (sr.rm_settings_txt_system_dirs,
                            seq_to_str(remove_selections.system_dirs,
                                       ", ", "", False))

        remove_selections.rm_system_dirs = is_yes(txt, False)

        # users dirs
        print

        txt = "%s (%s)?" % (sr.rm_settings_txt_user_dirs,
                            seq_to_str(remove_selections.user_dirs,
                                       ", ", "", False))

        remove_selections.rm_user_dirs = is_yes(txt, False)
        if remove_selections.rm_user_dirs:

            # read the users
            scan_seq(sr.users_subtitle,
                     'user',
                     sorted(all_users), # convert dictionary into an indexable list
                     remove_selections.users)

        # ask about backups if something is to be removed
        if remove_selections.have_remove_dirs():
            print
            txt = "%s in %s?" % (sr.rm_settings_txt_make_backups,
                                 remove_selections.backups_dir)
            remove_selections.make_backups = is_yes(txt)

        print "\n"

        return remove_selections

    def __show_summary(self, cmd, s, users_selections):
        '''Shows summary and asks if the user acceps to start installation or
        removal with these settings. Returns True if the user accepts, False if
        the wants to change the selections'''
        print s.summary_title + ":\n"
        print_wrapped(s.summary_subtitle)
        print

        print users_selections.get_summary_text()
        
        answer_accept = cmd.name
        answer_change = 'change'

        info = get_wrapped("Enter '%s' to begin %s with these settings" %
                           (answer_accept, s.name_noun)) + "\n"
        info += get_wrapped("Enter '%s' to change these settings" %
                            (answer_change))

        # loop until got valid answer from the user
        answer = ''
        while True:
            print
            print info

            answer = raw_input("> ")

            if answer == answer_accept:

                # if removing directories and backups will not be made, verify
                # if should proceed
                if (cmd.is_remove() and
                    users_selections.have_remove_dirs_no_backups()):

                    sure = is_yes(sr.summary_txt_no_backups_verify, False)

                    if sure:
                        return True

                    # if user is not sure (sure == False) then this loop will
                    # continue

                else:
                    return True

            elif answer == answer_change:
                return False

class QtUI(UI):
    '''The Qt user interface class'''

    def __init__(self):
        '''Constructor. 

        Raises FriendlyException if fails to initialize. The message of the
        raised exception will have the description of the problem.
 
        pkg_mgr = package manager used to install PyQt4 bindings, and passed
        to installation routines'''

        try:
            self.__create_wizard_classes()

        except AttributeError, e:
            MSGR.log("Traceback of handled exception")
            MSGR.log_exc()
            raise FriendlyException(
                "The version of Qt and PyQt bindings are too old. In order "
                "to use the GUI mode, please install Qt 4.3 or later and the "
                "respective python bindings.")

    def __create_wizard_classes(self):
        '''Creates Qt Wizard classes

        Can raise AttributeError if the version of PyQt4 is prior to Qt 4.3,
        since according to
        http://doc.trolltech.com/4.4/porting4-overview.html#wizard-dialogs-qwizard
        QWizard and siblings were added to that version of Qt4. Systems that have
        too old Qt and bindings by default: Debian Etch.'''

        QtGui = IMPORTER["PyQt4.QtGui"]
        QtCore = IMPORTER["PyQt4.QtCore"]

        class IntroPage(QtGui.QWizardPage):
            '''Introduction page'''

            def __init__(self, s):
                '''Constructor'''

                QtGui.QWizardPage.__init__(self)

                self.setTitle(s.intro_title)

                intro_label = QtGui.QLabel(s.intro_subtitle)
                intro_label.setWordWrap(True)

                layout = QtGui.QVBoxLayout()
                layout.addWidget(intro_label)

                self.setLayout(layout)

        class UsersPage(QtGui.QWizardPage):
            '''User selection page'''

            def __init__(self, s, all_users, selected_users):
                '''Constructor
                all_users  = list of all usernames (the user will have to chose
                             from this list)
                selected_users = set of usernames selected by the user,
                                 initially can contain default selection
                '''

                QtGui.QWizardPage.__init__(self)

                self.all_users = all_users
                self.selected_users = selected_users

                self.setTitle(s.users_title)
                self.setSubTitle(s.users_subtitle)

                self.users_list_widget = QtGui.QListWidget()

                txt = "List of users on the system. Rootstraps will be installed for selected users."
                self.users_list_widget.setToolTip(txt)
                self.users_list_widget.setWhatsThis(txt)

                # add users to the list
                for user in sorted(self.all_users):
                    item = QtGui.QListWidgetItem(user, self.users_list_widget)

                    item.setFlags(QtCore.Qt.ItemIsUserCheckable |
                                  QtCore.Qt.ItemIsEnabled)

                    # select/check the default users
                    if user in self.selected_users:
                        item.setCheckState(QtCore.Qt.Checked)
                    else:
                        item.setCheckState(QtCore.Qt.Unchecked)

                all_button = QtGui.QPushButton("&All")
                txt = "Selects all users"
                all_button.setToolTip(txt)
                all_button.setWhatsThis(txt)

                none_button = QtGui.QPushButton("N&one")
                txt = "Unselects all users"
                none_button.setToolTip(txt)
                none_button.setWhatsThis(txt)

                self.connect(none_button, QtCore.SIGNAL("clicked()"),
                             self.uncheckAllUsers)
                self.connect(all_button, QtCore.SIGNAL("clicked()"),
                             self.checkAllUsers)

                layout = QtGui.QGridLayout()
                layout.addWidget(self.users_list_widget, 0, 0, 1, 2)
                layout.addWidget(all_button, 1, 0, 1, 1)
                layout.addWidget(none_button, 1, 1, 1, 1)

                self.setLayout(layout)

            def validatePage(self):
                '''Overrides the method of QWizardPage, to read the selected
                users'''
                self.selected_users.clear()

                # loop through items in the list and store selected ones
                for i in range(self.users_list_widget.count()):
                    item = self.users_list_widget.item(i)

                    if item.checkState() == QtCore.Qt.Checked:
                        self.selected_users.add(str(item.text()))

                return True

            def allUsersSetCheckState(self, state):
                '''Sets the CheckState of all users in the list widget as
                specified'''
                for i in range(self.users_list_widget.count()):
                    item = self.users_list_widget.item(i)
                    item.setCheckState(state)

            def checkAllUsers(self):
                '''Selects all users'''
                self.allUsersSetCheckState(QtCore.Qt.Checked)

            def uncheckAllUsers(self):
                '''Unselects all users'''
                self.allUsersSetCheckState(QtCore.Qt.Unchecked)

        class RootstrapsPage(QtGui.QWizardPage):
            '''Rootstrap selection page'''

            def __init__(self, s, all_rootstraps, selected_rootstraps):
                '''Constructor
                all_rootstraps  = list of all rootstraps (the user will have
                                  to chose from this list)
                selected_rootstraps = set of rootstraps selected by the user,
                                      initially can contain default selection

                '''
                QtGui.QWizardPage.__init__(self)

                self.all_rootstraps = all_rootstraps
                self.selected_rootstraps = selected_rootstraps

                self.__createColumnsOrder()

                self.setTitle(s.rootstraps_title)
                self.setSubTitle(s.rootstraps_subtitle)

                self.rs_table_widget = QtGui.QTableWidget() # table of rootstraps

                self.rs_table_widget.setRowCount(len(self.all_rootstraps)) # number of rootstraps

                self.rs_table_widget.setColumnCount(len(self.columns_order))

                self.rs_table_widget.horizontalHeader().setResizeMode(QtGui.QHeaderView.ResizeToContents)
                self.rs_table_widget.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
                self.rs_table_widget.verticalHeader().hide()

                self.rs_table_widget.setHorizontalHeaderLabels([i.capitalize() for i in self.columns_order])

                txt = "Table of rootstraps that can be installed for each user"
                self.rs_table_widget.setToolTip(txt)
                self.rs_table_widget.setWhatsThis(txt)

                for row, rootstrap_name in enumerate(sorted(self.all_rootstraps)):
                    self.rsTableWidgetSetRow(row, self.all_rootstraps[rootstrap_name])

                all_button = QtGui.QPushButton("&All")
                txt = "Selects all rootstraps"
                all_button.setToolTip(txt)
                all_button.setWhatsThis(txt)

                none_button = QtGui.QPushButton("N&one")
                txt = "Unselects all rootstraps"
                none_button.setToolTip(txt)
                none_button.setWhatsThis(txt)

                self.connect(none_button, QtCore.SIGNAL("clicked()"),
                             self.uncheckAllRootstraps)
                self.connect(all_button, QtCore.SIGNAL("clicked()"),
                             self.checkAllRootstraps)

                layout = QtGui.QGridLayout()

                layout.addWidget(self.rs_table_widget, 0, 0, 1, 2)
                layout.addWidget(all_button, 1, 0, 1, 1)
                layout.addWidget(none_button, 1, 1, 1, 1)

                self.setLayout(layout)

            def __createColumnsOrder(self):
                '''Creates columns order for the rootstraps table'''

                # text identifying the selection column
                self.check_box_column_text = 'install'

                # order of columns in the rootstraps table, has only chosen
                # items from the rootstrap index file
                self.columns_order = [
                    # this column is not in rootstrap index file, it is used
                    # by a selection checkbox
                    self.check_box_column_text,
                    "name",
                    "label",
                    "date",
                    "size"
                ]

            def validatePage(self):
                '''Overrides the method of QWizardPage, to read the selected
                rootstraps'''
                self.selected_rootstraps.clear()

                for row in range(self.rs_table_widget.rowCount()):
                    cbx_item = self.rs_table_widget.item(row, self.columns_order.index(self.check_box_column_text))
                    name_item = self.rs_table_widget.item(row, self.columns_order.index("name"))

                    if cbx_item.checkState() == QtCore.Qt.Checked:
                        self.selected_rootstraps.add(str(name_item.text()))

                return True

            def allRootstrapsSetCheckState(self, state):
                '''Sets the CheckState of all rootstraps in the table as
                specified'''
                for row in range(self.rs_table_widget.rowCount()):
                    cbx_item = self.rs_table_widget.item(row, self.columns_order.index(self.check_box_column_text))
                    cbx_item.setCheckState(state)

            def checkAllRootstraps(self):
                '''Selects all rootstraps'''
                self.allRootstrapsSetCheckState(QtCore.Qt.Checked)

            def uncheckAllRootstraps(self):
                '''Unselects all rootstraps'''
                self.allRootstrapsSetCheckState(QtCore.Qt.Unchecked)

            def rsTableWidgetSetRow(self, row, rootstrap):
                '''Fills table row with the rootstrap information'''

                # add info to columns from of the specified row based on the rootstrap
                for clmn_index, clmn_title in enumerate(self.columns_order):

                    # install column has checkable items
                    if clmn_title == self.check_box_column_text:

                        item = QtGui.QTableWidgetItem()
                        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)

                        # check the rootstrap if it is one of the chosen ones
                        if rootstrap["name"] in self.selected_rootstraps:
                            item.setCheckState(QtCore.Qt.Checked)
                        else:
                            item.setCheckState(QtCore.Qt.Unchecked)

                        self.rs_table_widget.setItem(row, clmn_index, item)

                    # other columns have just text items
                    else:
                        if clmn_title == "size":
                            text = "%.1f MB" % (int(rootstrap['size']) / 1024 / 1024)

                        else:
                            text = rootstrap[clmn_title]

                        item = QtGui.QTableWidgetItem(text)
                        item.setFlags(QtCore.Qt.ItemIsEnabled)
                        self.rs_table_widget.setItem(row, clmn_index, item)

        class LicensePage(QtGui.QWizardPage):
            '''EUSA page'''

            def __init__(self, s):
                '''Constructor'''

                QtGui.QWizardPage.__init__(self)

                self.setTitle(s.license_title)
                self.setSubTitle(s.license_subtitle)

                license_text_edit = QtGui.QTextEdit()
                license_text_edit.setPlainText(EUSA_TEXT)
                license_text_edit.setReadOnly(True)

                txt = "Nokia Binaries EUSA. Use Ctrl+Wheel to zoom the text."
                license_text_edit.setToolTip(txt)
                license_text_edit.setWhatsThis(txt)

                self.accept_check_box = QtGui.QCheckBox("&I accept the terms of the license")
                self.accept_check_box.setCheckState(QtCore.Qt.Unchecked)

                layout = QtGui.QVBoxLayout()
                layout.addWidget(license_text_edit)
                layout.addWidget(self.accept_check_box)
                self.setLayout(layout)

            def initializePage(self):
                '''Overrides the method of QWizardPage, to initialize the page
                with some default settings.'''
                if self.wizard().users_selections.eusa_accepted:
                    self.accept_check_box.setCheckState(QtCore.Qt.Checked)
                else:
                    self.accept_check_box.setCheckState(QtCore.Qt.Unchecked)

            def validatePage(self):
                '''Overrides the method of QWizardPage, to write the
                license acceptance status'''
                self.wizard().users_selections.eusa_accepted = \
                    self.accept_check_box.checkState() == QtCore.Qt.Checked

                return True

        class SummaryPage(QtGui.QWizardPage):
            '''Summary page'''

            def __init__(self, s):
                '''Constructor'''

                QtGui.QWizardPage.__init__(self)

                self.s = s

                self.setTitle(s.summary_title)
                self.setSubTitle(s.summary_subtitle)

                # pages after this will have back button disabled
                self.setCommitPage(True)

                self.summary_label = QtGui.QLabel()
                self.summary_label.setWordWrap(True)
                self.summary_label.setAlignment(QtCore.Qt.AlignLeft |
                                                QtCore.Qt.AlignTop)

                # scroll area is used to add a scrolling capability in case the
                # text in the label becomes too long
                scroll_area = QtGui.QScrollArea(self)
                scroll_area.setWidgetResizable(True)
                scroll_area.setFrameStyle(QtGui.QFrame.NoFrame)
                scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
                scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

                # widget placed in scroll area
                sa_widget = QtGui.QWidget()

                # layout for the widget
                sa_widget_layout = QtGui.QVBoxLayout(sa_widget)
                sa_widget_layout.addWidget(self.summary_label)

                scroll_area.setWidget(sa_widget)

                # layout for the page
                layout = QtGui.QVBoxLayout()
                layout.addWidget(scroll_area)
                self.setLayout(layout)

            def initializePage(self):
                '''Overrides the method of QWizardPage, to initialize the page
                with summary text.'''

                self.wizard().setButtonText(QtGui.QWizard.CommitButton, 
                                            "&%s %s" %
                                            (self.s.name_verb_simple_present.capitalize(),
                                             PRODUCT_NAME_SHORT))

                summary = self.wizard().users_selections.get_summary_text(
                    '<br>', '<b>', '</b>')
                self.summary_label.setText(summary)

            def validatePage(self):
                '''Overrides the method of QWizardPage'''

                # if removing directories and backups will not be made, verify
                # if should proceed
                if (self.wizard().cmd.is_remove() and
                    self.wizard().users_selections.have_remove_dirs_no_backups()):

                    reply = QtGui.QMessageBox.warning(self,
                                                      "Directories will be removed!",
                                                      sr.summary_txt_no_backups_verify,
                                                      QtGui.QMessageBox.Yes |
                                                      QtGui.QMessageBox.No,
                                                      QtGui.QMessageBox.No)

                    if reply == QtGui.QMessageBox.No:
                        return False

                return True

        class ThreadSafeAnswer(object):
            '''Thread-safe answer to the questions asked in the GUI.  Used to
            pass the answer to the question asked in the main (GUI) thread to
            the executor thread.'''

            def __init__(self):
                '''Constructor'''

                self.__null_answer = None
                self.__answer = self.__null_answer

                # mutex protects access to answer
                self.__mutex = QtCore.QMutex()

                self.__answerSet = QtCore.QWaitCondition()

            def set(self, answer):
                '''Sets the answer'''
                locker = QtCore.QMutexLocker(self.__mutex)
                self.__answer = answer
                self.__answerSet.wakeAll()

            def __get(self):
                '''Returns the answer'''
                locker = QtCore.QMutexLocker(self.__mutex)

                if self.__answer == self.__null_answer:
                    self.__answerSet.wait(self.__mutex)

                return self.__answer

            def getAndNull(self):
                '''Reads the answer and sets it to null. Returns the answer.'''
                answer = self.__get()
                self.set(self.__null_answer)
                return answer

        class ExecutorThread(QtCore.QThread):
            '''Thread that executes the installation or removal functions.
            This is not done in the GUI thread not to freeze the GUI.

            Because in GUI applications, the main thread (a.k.a. the GUI
            thread) is the only thread that is allowed to perform GUI-related
            operations, signals are sent from this thread to the main thread to
            say or ask something from the user.'''

            def __init__(self, ts_answer, func, *args, **kwds):
                '''Constructor
                ts_answer = answer to the asked question from the GUI thread
                func = Function to execute in the thread
                args = Arguments to pass to func
                kwds = Keyword arguments to pass to func'''

                QtCore.QThread.__init__(self)
                self.ts_answer = ts_answer
                self.func = func
                self.args = args
                self.kwds = kwds

                # signal emitted when func says something
                self.sig_say = QtCore.SIGNAL("say(const QString&)")

                # signal emitted when func asks something
                # short-circuit signal another option is PyQt_PyObject
                self.sig_ask = QtCore.SIGNAL("ask")

            def run(self):
                '''Runs the installation or removal function'''
                MSGR.set_override_say(self.say)
                MSGR.set_override_ask(self.ask)
                self.func(*self.args, **self.kwds)
                MSGR.remove_override_ask()
                MSGR.remove_override_say()

            def say(self, msg):
                '''Overrides the default say functionality of the Messenger, to
                emit a signal so that UI could be updated with new info in the
                GUI thread'''
                self.emit(self.sig_say, msg)

            def ask(self, title, question, choices, default_choice):
                '''Overrides the default ask functionality of the Messenger.
                Emits a signal so that the GUI thread could ask the question,
                then reads the answer from the GUI thread.'''
                self.emit(self.sig_ask, title, question, choices, default_choice)
                answer = self.ts_answer.getAndNull()

                return answer

        class ProgressPage(QtGui.QWizardPage):
            '''Progress page'''

            def __init__(self, s):
                '''Constructor'''

                QtGui.QWizardPage.__init__(self)

                self.s = s
                self.ts_answer = ThreadSafeAnswer()

                # whether the slider in the text widget has been scrolled to
                # its vertical scroll bars end, it will be scrolled only once
                # at initialization; when the slider is scrolled to the
                # vertical end, the newly appended text will cause automatic
                # scrolling, so the new text will always be visible
                self.scrolled_to_vend = False

                self.setTitle(s.progress_title)
                self.setSubTitle(s.progress_subtitle)

                self.status_label = QtGui.QLabel("%s..." % (s.name_verb_present_continuous.capitalize()))
                self.status_label.setWordWrap(True)

                # If minimum and maximum both are set to 0, the bar shows a
                # busy indicator instead of a percentage of steps.
                self.progress_bar = QtGui.QProgressBar()
                self.progress_bar.setMinimum(0)
                self.progress_bar.setMaximum(0)

                self.logs_text_edit = QtGui.QTextEdit()

                txt = "Logs from %s.  Use Ctrl+Wheel to zoom the text." % MSGR.fn_log
                self.logs_text_edit.setToolTip(txt)
                self.logs_text_edit.setWhatsThis(txt)

                # set proper coloring of the text edit
                # must be done before writing any text, otherwise black text
                # will not be visible once you change background to black
                #
                # new in Qt 4.4: not used now, since some systems won't have it
                # self.logs_text_edit.setTextBackgroundColor(QtCore.Qt.black)
                palette = QtGui.QPalette()
                palette.setColor(QtGui.QPalette.Active, QtGui.QPalette.Base, QtCore.Qt.black);
                palette.setColor(QtGui.QPalette.Inactive, QtGui.QPalette.Base, QtCore.Qt.black);
                self.logs_text_edit.setPalette(palette);
                self.logs_text_edit.setTextColor(QtCore.Qt.green)

                # there must be some text in the editor, otherwise the color of
                # text written with cursor will be black, why? go figure...
                self.logs_text_edit.setPlainText("logs from %s\n" % MSGR.fn_log)

                self.logs_text_edit.setReadOnly(True)

                # to look like a proper terminal use fixed width fonts
                self.logs_text_edit.setFont(QtGui.QFont("Monospace", 10))

                # cursor is used to append text to the end of the edit widget,
                # there is append method but it adds extra newline, and
                # InsertPlainText method inserts text at current cursor
                # position, which can be changed by the user just by clicking
                # somewhere in the edit widget
                self.cursor = self.logs_text_edit.textCursor()
                self.cursor.movePosition(QtGui.QTextCursor.End)

                self.logs_button = QtGui.QPushButton()
                txt = "Toggles visibility of the logs view"
                self.logs_button.setToolTip(txt)
                self.logs_button.setWhatsThis(txt)
                self.logs_button.setCheckable(True)
                self.connect(self.logs_button,
                             QtCore.SIGNAL("toggled(bool)"),
                             self.logsButtonToggled)

                # takes the space of text edit when it is hidden
                self.spacer = QtGui.QSpacerItem(0, 0,
                                                QtGui.QSizePolicy.Minimum,
                                                QtGui.QSizePolicy.Expanding)

                # process that tails the log file
                self.tail_process = QtCore.QProcess(self)
                self.tail_process.start("tail -f " + MSGR.fn_log)
                self.connect(self.tail_process,
                             QtCore.SIGNAL("readyReadStandardOutput()"),
                             self.tailProcessReadStdout)

                layout = QtGui.QVBoxLayout()
                layout.addWidget(self.status_label)
                layout.addWidget(self.progress_bar)
                # spacer will be removed in button's toggled signal handler
                layout.addItem(self.spacer)
                layout.addWidget(self.logs_text_edit)
                self.setLayout(layout)

                self.logs_button.setChecked(True) # will emit toggled signal

            def initializePage(self):
                '''Overrides the method of QWizardPage, to initialize the page
                with some default settings.'''

                QtGui.QApplication.setOverrideCursor(QtCore.Qt.BusyCursor)

                self.executor = ExecutorThread(self.ts_answer,
                                               self.wizard().cmd.run,
                                               self.wizard().users_selections,
                                               self.wizard().pkg_mgr,
                                               self.wizard().all_users,
                                               self.wizard().all_rootstraps)

                # this will update the text of the status label with new info
                self.connect(self.executor, self.executor.sig_say,
                             self.status_label,
                             QtCore.SLOT("setText(const QString&)"))

                self.connect(self.executor, self.executor.sig_ask, self.ask)

                self.connect(self.executor, QtCore.SIGNAL("finished()"),
                             self.executorDone)

                self.executor.start()

                # have a custom button to show/hide logs
                self.wizard().setOption(QtGui.QWizard.HaveCustomButton1, True)
                self.wizard().setButton(QtGui.QWizard.CustomButton1, self.logs_button)

            def validatePage(self):
                '''Overrides the method of QWizardPage, to remove the custom
                button.'''
                self.wizard().setOption(QtGui.QWizard.HaveCustomButton1, False)

                # QWizard deletes the old button when setButton is used
                self.wizard().setButton(QtGui.QWizard.CustomButton1, None)

                return True

            def isBusy(self):
                '''Returns True if installation/removal is in progress'''
                return self.executor.isRunning()

            def executorDone(self):
                '''Called when executor thread has finished its jobs. This
                means installation or removal is complete at this point. So
                this routine reflects that to the UI'''

                self.setSubTitle(self.s.progress_subtitle_done)

                # show the result in the progress bar
                if MSGR.get_fatal():
                    value = 0
                elif MSGR.get_errors(): # some non-critical errors
                    value = 5
                else: # success
                    value = 10

                self.progress_bar.setRange(0, 10)
                self.progress_bar.setValue(value)

                # print error info
                if MSGR.get_fatal():

                    # print fatal error in the label
                    self.status_label.setText(
                        "<font color=red><b>Failed: %s</font></b>" %
                        MSGR.get_fatal())

                    # message box with fatal error
                    QtGui.QMessageBox.critical(self, "Fatal Error",
                                               sc.fatal_title + "\n%s" %
                                               MSGR.get_fatal())

                if MSGR.get_errors():
                     txt = sc.errors_title + '\n'

                     for err in MSGR.get_errors():
                         txt += err + "\n"

                     QtGui.QMessageBox.warning(self, "Warning", txt)

                # enable the next button
                self.emit(QtCore.SIGNAL("completeChanged()"))

                # from this page on there is nothing to cancel, so disabled
                self.wizard().button(QtGui.QWizard.CancelButton).setEnabled(False)

                QtGui.QApplication.restoreOverrideCursor()

            def isComplete(self):
                '''Overrides the method of QWizardPage, to disable next button
                when installation/removal is in progress.'''
                if self.isBusy():
                    return False
                else:
                    return True

            def onCancel(self):
                '''Called if user tries to close or cancel the wizard'''
                QtGui.QMessageBox.information(
                    self, "Sorry!", "Cancel is not implemented, yet...")

                # TODO: call self.wizard().reject() if user wants to cancel
                # (must kill installation thread first)

            def showEvent(self, event):
                '''Overriden method of QWidget. Scrolls the slider of the
                vertical scroll bar of the text edit to the end.  This is done
                to enable automatic scrolling of the scrollbar upon addition
                of the new text.  Done only once, when the page is shown for
                the first time.'''
                QtGui.QWizardPage.showEvent(self, event)

                if not self.scrolled_to_vend:
                    self.logsTextEditScrollToVend()
                    self.scrolled_to_vend = True

            def logsButtonToggled(self, checked):
                '''Slot for the toggled signal of the logs button. Hides/shows
                logs text edit.'''
                self.logs_text_edit.setVisible(checked)

                if checked:
                    self.logs_button.setText("Hide &logs")

                    # when text edit is visible spacer is removed, otherwise
                    # spacer would visibly consume space if page is resized
                    self.layout().removeItem(self.spacer)

                else:
                    self.logs_button.setText("Show &logs")

                    # when text edit is hidden the spacer is used to take its
                    # space to prevent other widget from moving in the layout
                    self.layout().insertItem(2, self.spacer)

            def logsTextEditScrollToVend(self):
                '''Scrolls vertical scroll bar of the text edit widget to
                the end'''
                vsb = self.logs_text_edit.verticalScrollBar()
                vsb.setValue(vsb.maximum())

            def logsTextEditRemoveLastLine(self):
                '''Removes last line from the text edit'''
                cursor = self.logs_text_edit.textCursor()
                cursor.movePosition(QtGui.QTextCursor.End)
                cursor.select(QtGui.QTextCursor.BlockUnderCursor)
                cursor.deletePreviousChar()

            def splitStringWithMultiCR(self, txt):
                '''Splits a string containing multiple lines of text into list
                a of single-line strings. Character \r is assumed to mark the
                beginning of a line, whereas character \n is assumed to mark
                the end of a line. This routine is used to emulate terminal
                carriage return (\r) handling.

                returns a list of strings
                txt = a string containing multiple lines of text (including \n,
                      \r etc)'''
                txt_list = []

                i_begin = 0 # index, where the next line should begin from

                for i in xrange(0, len(txt)):

                    # \r is assumed to mark the beginning of a new line
                    if txt[i] == '\r':

                        # there could be \r just after line ending with \n or
                        # the whole text could begin with \r: we don't want
                        # empty string in those cases
                        if i - i_begin > 0:
                            txt_list.append(txt[i_begin:i])
                            i_begin = i

                    # \n is assumed to mark the end of a current line, next
                    # char after newline will be the start of the next line
                    elif txt[i] == '\n':
                        txt_list.append(txt[i_begin:i + 1])
                        i_begin = i + 1

                # if text does not end with \n, get the last line
                if i_begin < len(txt):
                    txt_list.append(txt[i_begin:len(txt)])

                return txt_list

            def logsTextEditAppend(self, txt):
                '''Appends text into the text edit widget. If there are
                carriage return characters in the text does some processing in
                order to emulate terminal behavior.

                txt = a string containing multiple lines of text (including \n,
                      \r etc)'''
                vsb = self.logs_text_edit.verticalScrollBar()

                at_bottom = vsb.value() == vsb.maximum()

                # number of carriage return characters in the text
                cr_count = txt.count('\r')

                # text without carriage return is just inserted
                if cr_count == 0:
                    self.cursor.insertText(txt)

                # text has only one carriage return in the beginning, previous
                # line must be removed before the text is inserted
                elif cr_count == 1 and txt.startswith('\r'):
                    self.logsTextEditRemoveLastLine() 
                    self.cursor.insertText(txt)

                # text has multiple carriage return characters
                else:
                    txt_list = self.splitStringWithMultiCR(txt)
                    for line in txt_list:
                        if line.startswith('\r'):
                            self.logsTextEditRemoveLastLine()
                        self.cursor.insertText(line)

                # automatically scroll, if slider was at the end of scrollbar
                if at_bottom:
                    self.logsTextEditScrollToVend()

            def tailProcessReadStdout(self):
                '''A slot that is called when the tail process has some text on
                its stdout. Appends that text to the text edit.'''
                txt = str(self.tail_process.readAllStandardOutput())
                self.logsTextEditAppend(txt)

            def ask(self, title, question, choices, default_choice):
                '''Asks question from the user'''

                # expand on demand
                choice_to_button = { AskChoiceRetry : QtGui.QMessageBox.Retry,
                                     AskChoiceAbort : QtGui.QMessageBox.Abort }

                # find out the buttons
                buttons = 0
                for i in choices:
                    buttons |= choice_to_button[i]

                default_button = choice_to_button[default_choice]

                reply = QtGui.QMessageBox.warning(self, title, question,
                                                  buttons, default_button)

                # get the choice from button
                for k, v in choice_to_button.items():
                    if v == reply:
                        result = k
                        break

                self.ts_answer.set(result)

        class ConclusionPage(QtGui.QWizardPage):
            '''ConclusionPage page'''

            def __init__(self, s):
                '''Constructor'''

                QtGui.QWizardPage.__init__(self)

                self.setTitle(s.conclusion_title)

                label = QtGui.QLabel(s.conclusion_subtitle)
                label.setWordWrap(True)

                layout = QtGui.QVBoxLayout()
                layout.addWidget(label)
                self.setLayout(layout)

        class RmSettingsPage(QtGui.QWizardPage):
            '''Remove settings page'''

            def __init__(self, s):
                '''Constructor'''

                QtGui.QWizardPage.__init__(self)

                self.setTitle(s.rm_settings_title)
                self.setSubTitle(s.rm_settings_subtitle)

                self.rm_system_dirs_cbx = QtGui.QCheckBox(s.rm_settings_txt_system_dirs_acc)
                self.rm_user_dirs_cbx = QtGui.QCheckBox(s.rm_settings_txt_user_dirs_acc)
                self.make_backups_cbx = QtGui.QCheckBox(s.rm_settings_txt_make_backups_acc)

                self.connect(self.rm_system_dirs_cbx,
                             QtCore.SIGNAL("toggled(bool)"),
                             self.updateBackupsCbx)

                self.connect(self.rm_user_dirs_cbx,
                             QtCore.SIGNAL("toggled(bool)"),
                             self.updateBackupsCbx)

                layout = QtGui.QVBoxLayout()
                layout.addWidget(self.rm_system_dirs_cbx)
                layout.addWidget(self.rm_user_dirs_cbx)
                layout.addWidget(self.make_backups_cbx)
                self.setLayout(layout)

            def initializePage(self):
                '''Overrides the method of QWizardPage, to initialize the page
                with some default settings.'''

                self.rm_system_dirs_cbx.setChecked(
                    self.wizard().users_selections.rm_system_dirs)

                self.rm_user_dirs_cbx.setChecked(
                    self.wizard().users_selections.rm_user_dirs)

                self.make_backups_cbx.setChecked(
                    self.wizard().users_selections.make_backups)


                # help text is set in this method, since self.wizard() is not
                # available in the constructor
                txt = "Check to remove from system:\n%s" % seq_to_str(
                    self.wizard().users_selections.system_dirs, ", ", "", False)
                self.rm_system_dirs_cbx.setToolTip(txt)
                self.rm_system_dirs_cbx.setWhatsThis(txt)

                txt = "Check to remove from users home:\n%s" % seq_to_str(
                    self.wizard().users_selections.user_dirs, ", ", "", False)
                self.rm_user_dirs_cbx.setToolTip(txt)
                self.rm_user_dirs_cbx.setWhatsThis(txt)

                txt = "Check to make backups in\n%s" % \
                    self.wizard().users_selections.backups_dir
                self.make_backups_cbx.setToolTip(txt)
                self.make_backups_cbx.setWhatsThis(txt)


                self.updateBackupsCbx(True) # arg is not used

            def validatePage(self):
                '''Overrides the method of QWizardPage, to update the user's
                selections'''
                self.wizard().users_selections.rm_system_dirs = \
                    self.rm_system_dirs_cbx.isChecked()

                self.wizard().users_selections.rm_user_dirs = \
                    self.rm_user_dirs_cbx.isChecked()

                self.wizard().users_selections.make_backups = \
                    self.make_backups_cbx.isChecked()

                return True

            def updateBackupsCbx(self, checked):
                '''Enables/disables backups checkbox. It is enabled if any of
                the remove checkboxes is checked, otherwise it is disabled.'''

                if self.rm_system_dirs_cbx.isChecked() or\
                        self.rm_user_dirs_cbx.isChecked():

                    self.make_backups_cbx.setEnabled(True)
                    self.make_backups_cbx.setChecked(True)
                else:
                    self.make_backups_cbx.setEnabled(False)
                    self.make_backups_cbx.setChecked(False)

        class AddSwPage(QtGui.QWizardPage):
            '''Page to display additional software available for
            installation'''

            def __init__(self, s):
                '''Constructor'''

                QtGui.QWizardPage.__init__(self)

                self.setTitle(s.addsw_title)
                self.setSubTitle(s.addsw_subtitle)

                self.eclipse_ide_cbx = QtGui.QCheckBox(s.addsw_txt_eclipse_ide_acc)
                txt = "Eclipse-based IDE is the official IDE for %s" % PRODUCT_NAME
                self.eclipse_ide_cbx.setToolTip(txt)
                self.eclipse_ide_cbx.setWhatsThis(txt)

                layout = QtGui.QVBoxLayout()
                layout.addWidget(self.eclipse_ide_cbx)
                self.setLayout(layout)

            def initializePage(self):
                '''Overrides the method of QWizardPage, to initialize the page
                with some default settings.'''
                self.eclipse_ide_cbx.setChecked(
                    self.wizard().users_selections.install_eclipse_ide)

            def validatePage(self):
                '''Overrides the method of QWizardPage, to update the user's
                selections'''
                self.wizard().users_selections.install_eclipse_ide = \
                    self.eclipse_ide_cbx.isChecked()

                return True

        PageIdIntro      = 0
        PageIdRmSettings = 1 # used only by remove wizard
        PageIdUsers      = 2
        PageIdRootstraps = 3
        PageIdLicense    = 4
        PageIdAddSw      = 5 # used only by install wizard
        PageIdSummary    = 6
        PageIdProgress   = 7
        PageIdConclusion = 8

        class WizardBase(QtGui.QWizard):
            '''Base class for wizards, contains all common functionality.'''

            def __init__(self, cmd, pkg_mgr, all_users, all_rootstraps):
                '''Constructor'''

                QtGui.QWizard.__init__(self)

                self.cmd = cmd
                self.pkg_mgr = pkg_mgr
                self.all_users = all_users
                self.all_rootstraps = all_rootstraps

                self.users_selections = None
                self.s = None # class of strings

                self.setWindowTitle("%s (%s)" % (MY_NAME, self.cmd.name))

                # self.setOption(QtGui.QWizard.NoBackButtonOnStartPage)
                # self.setOption(QtGui.QWizard.NoBackButtonOnLastPage)
                self.setOption(QtGui.QWizard.DisabledBackButtonOnLastPage)

            def reject(self):
                '''Overridden method of QDialog to disable wizard closing when
                installation/removal is in progress. Handles closing wizard by:
                - pressing Esc button
                - clicking Cancel button
                - clicking X button on the title bar of the window
                - any shortcut that can be used to close a window
                - probably any other means used to close a window'''

                if self.currentId() == PageIdProgress and \
                        self.currentPage().isBusy():

                    self.currentPage().onCancel()
                    return

                # default behavior in other cases
                QtGui.QWizard.reject(self)

        class InstallWizard(WizardBase):
            '''Installation wizard'''

            def __init__(self, cmd, pkg_mgr, all_users, all_rootstraps):
                '''Constructor'''

                WizardBase.__init__(self, cmd, pkg_mgr, all_users,
                                    all_rootstraps)

                self.users_selections = InstallSelections(all_users,
                                                          all_rootstraps)
                self.s = si

                self.setPage(PageIdIntro, IntroPage(self.s))
                self.setPage(PageIdUsers, UsersPage(self.s, self.all_users, self.users_selections.users))
                self.setPage(PageIdRootstraps, RootstrapsPage(self.s, self.all_rootstraps, self.users_selections.rootstraps))
                self.setPage(PageIdLicense, LicensePage(self.s))
                if HAVE_ADDSW:
                    self.setPage(PageIdAddSw, AddSwPage(self.s))
                self.setPage(PageIdSummary, SummaryPage(self.s))
                self.setPage(PageIdProgress, ProgressPage(self.s))
                self.setPage(PageIdConclusion, ConclusionPage(self.s))

            def nextId(self):
                '''Returns ID of page to show when the user clicks the Next
                button.'''

                if self.currentId() == PageIdUsers:

                    # if users selected, give choice to select rootstraps
                    if self.users_selections.users:
                        return PageIdRootstraps

                    elif HAVE_ADDSW:
                        return PageIdAddSw

                    else:
                        return PageIdSummary
                    
                if self.currentId() == PageIdRootstraps:

                    # if rootstraps selected give choice to install Nokia
                    # Binaries into them
                    if self.users_selections.rootstraps:
                        return PageIdLicense

                    elif HAVE_ADDSW:
                        return PageIdAddSw

                    else:
                        return PageIdSummary

                # default behavior for other pages
                return WizardBase.nextId(self)

        class RemoveWizard(WizardBase):
            '''Removal wizard'''

            def __init__(self, cmd, pkg_mgr, all_users, all_rootstraps):
                '''Constructor'''

                WizardBase.__init__(self, cmd, pkg_mgr, all_users,
                                    all_rootstraps)

                self.users_selections = RemoveSelections(all_users,
                                                         all_rootstraps)
                self.s = sr

                self.setPage(PageIdIntro, IntroPage(self.s))
                self.setPage(PageIdRmSettings, RmSettingsPage(self.s))
                self.setPage(PageIdUsers, UsersPage(self.s, self.all_users, self.users_selections.users))
                self.setPage(PageIdSummary, SummaryPage(self.s))
                self.setPage(PageIdProgress, ProgressPage(self.s))
                self.setPage(PageIdConclusion, ConclusionPage(self.s))

            def nextId(self):
                '''Returns ID of page to show when the user clicks the Next
                button.'''

                if self.currentId() == PageIdRmSettings:

                    # selected to remove files in user's home: show Users page
                    if self.users_selections.rm_user_dirs:
                        return PageIdUsers

                    # otherwise, show summary
                    else:
                        return PageIdSummary

                # default behavior for other pages
                return WizardBase.nextId(self)

        self.wizard_classes = { CmdNameInstall : InstallWizard, CmdNameRemove : RemoveWizard }

    def run_cmd(self, cmd, pkg_mgr, all_users, all_rootstraps):
        '''See docstring of the same method of the parent class'''

        QtGui = IMPORTER["PyQt4.QtGui"]

        app = QtGui.QApplication([])

        wizard = self.wizard_classes[cmd.name](cmd,
                                               pkg_mgr,
                                               all_users,
                                               all_rootstraps)

        wizard.show()
        app.exec_()

def create_ui():
    '''UI factory function. Creates suitable UI object and returns it.
    Will return None in some error cases'''

    ui = None
    fallback_to_cmd_line_ui = False

    # GUI specified as option, try to create it
    if OPTIONS.gui:
        try:
            ui = QtUI()

        # failed to create GUI
        except FriendlyException, e:
            print_wrapped(str(e))
            MSGR.log_exc()

            fallback_to_cmd_line_ui = is_yes("Failed to start-up GUI, fall "
                                             "back to command line interface?")

    # create command line UI
    if not OPTIONS.gui or fallback_to_cmd_line_ui:
        ui = CmdLineUI()

    return ui

def get_all_rootstraps():
    '''Returns a dictionary of available rootstraps parsed from the rootstrap
    index file

    Downloads and reads the rootstrap index file. Returns a dictionary
    of rootstraps, with keys set to rootstrap names, and values to
    dictionaries of rootstrap properties. The parser is not returned,
    not to make the caller file format dependent.
    '''

    rsi_file = "index.rootstraps"
    rsi_url = "http://maemo-sdk.garage.maemo.org/download/rootstrap-index/"

    MSGR.say("Downloading rootstraps index file...")
    MSGR.log("Downloading rootstraps index file from %s" % rsi_url)

    rsi_fo = urllib2.urlopen(rsi_url + rsi_file) # file-like object

    parser = ConfigParser.ConfigParser()
    parser.readfp(rsi_fo)

    # convert parser into dictionary
    rs = {}
    for s in parser.sections():
        rs[parser.get(s, "name")] = dict(parser.items(s))

    return rs

def get_all_users():
    '''Returns non-system users if can get the system limits for normal user's
    UID. If not then returns all of the users in the system. The users are
    returned in a dictionary with user names as keys and password database
    entries (pwd.struct_passwd) as values'''
    user_conf_file = "/etc/adduser.conf"

    # inclusive range of UIDs for normal (non-system) users
    # by default this is the maximum possible range, that includes all users
    first_uid = 0
    last_uid = sys.maxint

    found_first_uid = False
    found_last_uid = False 

    # try to get the UID range
    if os.path.isfile(user_conf_file):
        for l in open(user_conf_file):

            l = l.strip()

            if l.startswith("FIRST_UID"):
                first_uid = int(l.split("=")[1])
                found_first_uid = True

            if l.startswith("LAST_UID"):
                last_uid = int(l.split("=")[1])
                found_last_uid = True

            # both found
            if found_first_uid and found_last_uid:
                break

    # make the users dictionary
    users = {}

    for i in pwd.getpwall():
        # if UID not within the range, then skip this user
        if not first_uid <= i.pw_uid <= last_uid:
            continue
        users[i.pw_name] = i

    return users

def remove_dir(dir):
    '''Removes the specified directory recursively.'''

    MSGR.say("Removing directory %s" % dir)

    if os.path.isdir(dir):
        shutil.rmtree(dir)
        MSGR.log("Removed directory %s" % dir)

    else:
        MSGR.log("Directory %s does not exist, not removed" % dir)

def set_eguid(pwd_ent):
    '''Changes the effective user and group IDs, should be used to temporarily
    drop root privileges. Also sets HOME environment variable since maemo-sdk
    command extensively uses that variable.

    pwd_ent = Password database entry of the user whose credentials will be
    used'''
    os.setegid(pwd_ent.pw_gid)
    os.seteuid(pwd_ent.pw_uid)
    os.environ['HOME'] = pwd_ent.pw_dir

def set_guid(pwd_ent):
    '''Changes the effective, real and saved set-user-ID user and group IDs,
    should be used to permanently drop root privileges. Also sets HOME
    environment variable since maemo-sdk command extensively uses that
    variable.

    pwd_ent = Password database entry of the user whose credentials will be
    used'''
    os.setgid(pwd_ent.pw_gid)
    os.setuid(pwd_ent.pw_uid)
    os.environ['HOME'] = pwd_ent.pw_dir

def make_unique_filename(name_prefix):
    '''Returns a unique (non-existent) file name
    name_prefix = path & beginning of the name'''
    result = name_prefix
    index = 1

    # loop until unique name found
    while True:

        # name used, try another name
        if os.path.exists(result):
            result = name_prefix + "." + str(index)
            index += 1

        # found unique name
        else:
            break

    return result

def rootstrap_install(user, rootstrap, install_nokia_binaries):
    '''Installs a rootstrap
    user = password entry for the user of type pwd.struct_passwd
    rootstrap = rootstrap index file entry
    install_nokia_binaries = if True Nokia binaries will be installed into
                             the rootstrap
    '''

    rs_name = rootstrap['name']

    MSGR.say("Installing rootstrap %s for user %s" % (rs_name, user.pw_name))

    # sb2-init (in sb2 version 1.99.0.25) is sh (dash) incompatible, so must
    # use bash (will be fixed in future sb2 releases)
    result = exec_cmd_error("bash -c 'maemo-rootstrap install %s'" % (rs_name,), user)

    # useless to install Nokia Binaries if failed to install rootstrap
    if result:
        MSGR.log("Won't install binaries since rootstrap installation failed!")
        return

    # return if no need to install Nokia Binaries
    if not install_nokia_binaries:
        return

    # to install Nokia Binaries must send 'I accept' to the binaries installer
    # if binaries installer output is not redirected then must also quit more
    # pager
    MSGR.say("Installing Nokia Binaries into the rootstrap %s for user %s"
             % (rs_name, user.pw_name))

    cmd = "maemo-rootstrap bin-install %s" % (rs_name,)
    MSGR.log("Executing: " + cmd)

    try:
        p = subprocess.Popen(cmd,
                             shell = True,
                             stdin = subprocess.PIPE,
                             stdout = MSGR.fo_log,
                             stderr = subprocess.STDOUT,
                             preexec_fn = lambda: set_guid(user))
        p.communicate("I accept\n")
        p.wait()

        if p.returncode:
            MSGR.add_error()

    except Exception, e:
        if hasattr(e, 'child_traceback'):
            print "child_traceback:"
            print e.child_traceback
            raise
    return

def toolsenvs_install(all_rootstraps, selected_rootstraps):
    '''Installs build tools environments that are needed by the selected
    rootstraps (according to the rootstraps index file).
    all_rootstraps = dictionary of all rootstraps with detailed info
    selected_rootstraps = set of rootstrap names'''

    # get installed toolsenvs
    installed_toolsenvs = subprocess.Popen(["maemo-sdk", "list", "tools"],
                                           stderr = subprocess.STDOUT,
                                           stdout = subprocess.PIPE
                                           ).communicate()[0].splitlines()

    MSGR.log("Found installed tools environments: %s" % installed_toolsenvs)

    # missing toolsenvs (must install to satisfy rootstrap installations)
    missing_toolsenvs = set()

    # get missing toolsenvs that are needed by selected rootstraps
    for rootstrap in selected_rootstraps:

        toolsenv = all_rootstraps[rootstrap]["tools"]

        # if toolsenv is not an empty string and it is not installed, then it
        # is missing
        if toolsenv and toolsenv not in installed_toolsenvs:
            missing_toolsenvs.add(toolsenv)

    MSGR.log("Will install missing tools environments: %s" % missing_toolsenvs)

    # install the missing toolsenvs
    for toolsenv in missing_toolsenvs:
        MSGR.say("Installing tools environment %s (takes about 15 minutes)" %
                 toolsenv)
        # raw string to log without newlines in effect
        exec_cmd_fatal(
            r"echo -e 'Yes\n' | maemo-tools --mirror %s install %s"\
                % (OPTIONS.tools_mirror, toolsenv))

def toolchains_install(all_rootstraps, selected_rootstraps):
    '''Installs missing toolchains. Some (default) toolchains are installed
    upon installation of the SDK packages (e.g. with apt). This routine
    installs toolchains that are not installed by default, and are needed
    by the selected rootstraps.
    all_rootstraps = dictionary of all rootstraps with detailed info
    selected_rootstraps = set of rootstrap names'''

    # get installed toolchains
    installed_toolchains = subprocess.Popen(["maemo-sdk", "list", "toolchains"],
                                            stderr = subprocess.STDOUT,
                                            stdout = subprocess.PIPE
                                            ).communicate()[0].splitlines()

    MSGR.log("Found installed toolchains: %s" % installed_toolchains)

    # missing toolchains (must install to satisfy rootstrap installations)
    missing_toolchains = set()

    # get missing toolchains that are needed by selected rootstraps
    for rootstrap in selected_rootstraps:

        toolchain = all_rootstraps[rootstrap]["toolchain"]

        # if toolchain is not an empty string and it is not installed, then it
        # is missing
        if toolchain and toolchain not in installed_toolchains:
            missing_toolchains.add(toolchain)

    MSGR.log("Will install missing toolchains: %s" % missing_toolchains)

    # install the missing toolchains
    for toolchain in missing_toolchains:
        MSGR.say("Installing toolchain %s" % toolchain)
        exec_cmd_fatal("maemo-tools install-toolchain %s" % toolchain)

def sdk_install(install_selections, pkg_mgr, all_users, all_rootstraps):
    '''Installs SDK'''

    MSGR.say("Installing %s. This might take a while..." % PRODUCT_NAME)
    MSGR.log("Starting installation with selections: %s" % (install_selections))

    pkg_mgr.sdk_install()

    # reload catalogues for each user individually
    if install_selections.users:
        for user in install_selections.users:
            MSGR.say("Initializing maemo-sdk for user %s" %  user)
            exec_cmd_fatal("maemo-sdk reload catalogue", all_users[user])

    # to be installed only if users and rootstraps are selected
    if install_selections.users and install_selections.rootstraps:

        toolsenvs_install(all_rootstraps, install_selections.rootstraps)
        toolchains_install(all_rootstraps, install_selections.rootstraps)

        # install rootstrap for users: the last rootstrap installed is the
        # default target
        for user in install_selections.users:
            for rootstrap in install_selections.rootstraps:
                rootstrap_install(all_users[user],
                                  all_rootstraps[rootstrap],
                                  install_selections.eusa_accepted)
    else:
        MSGR.log("Not installing rootstraps and dependencies, because "
                 "unspecified (%s)(%s)" %
                 (install_selections.users, install_selections.rootstraps))

    # install the Eclipse-based IDE
    if HAVE_ADDSW and install_selections.install_eclipse_ide:
        pkg_mgr.pkg_install(pkg_mgr.pkg_name_eclipse_ide, False)

    # if there is fatal this place isn't reached
    if MSGR.get_errors():
        MSGR.say("%s was installed despite some non-critical errors" % PRODUCT_NAME)
    else:
        MSGR.say("%s was successfully installed" % PRODUCT_NAME)

def make_backups(backups_dir, dirs):
    '''Makes a tarball of the specfied directories in the backups directory
    backups_dir = directory where to create the backup tarball
    dirs = list directories to be written into the tarball
    '''
    # make backups directory if does not exist
    if not os.path.exists(backups_dir):
        MSGR.say("Making backups directory %s" % backups_dir)
        os.mkdir(backups_dir)

    # make a file name for the tarball
    parent_dir = time.strftime("backup_by_installer_%Y%m%d_%H%M%S")
    tar_filename = parent_dir + ".tar.gz"
    tar_filename = os.path.join(backups_dir, tar_filename)
    tar_filename = make_unique_filename(tar_filename)

    MSGR.say("Writing backups into %s" % tar_filename)

    # make the tarball and add dirs into it
    tar = tarfile.open(tar_filename, "w:gz")

    for dir in dirs:
        if os.path.exists(dir):
            MSGR.say("Making backup copy of %s" % dir)
            tar.add(dir, parent_dir + dir)

    tar.close()

def sdk_remove(remove_selections, pkg_mgr, all_users, all_rootstraps):
    '''Removes the maemo-sdk'''

    MSGR.say("Removing %s. This might take a while..." % PRODUCT_NAME)
    MSGR.log("Starting removal with selections: %s" % (remove_selections))

    # remove system wide stuff

    pkg_mgr.sdk_remove()

    remove_dirs = remove_selections.get_remove_dirs(True)

    if remove_dirs:
        # backup
        if remove_selections.make_backups:
            make_backups(remove_selections.backups_dir, remove_dirs)
        
        # remove
        for dir in remove_dirs:
            remove_dir(dir)

    # if there is fatal this place isn't reached
    if MSGR.get_errors():
        MSGR.say("%s was removed despite some non-critical errors" % PRODUCT_NAME)
    else:
        MSGR.say("%s was successfully removed" % PRODUCT_NAME)

def check_system(sys_info):
    '''Checks if the system is supported by this script
    Returns True if system is supported, False otherwise.'''

    MSGR.log("Checking system for compatibility: %s" % sys_info)

    # check OS: currently only Linux is supported
    if not sys_info.platform.startswith("linux"):
        print "Operating system (%s) is not supported. " % sys_info.platform
        print "Only Linux is supported currently."
        return False

    # only 32 bit systems are supported
    sup_machine = ["i386", "i686"]

    # check machine
    if sys_info.machine not in sup_machine:
        print "Operating system's machine or word size (%s) is not supported."\
            % sys_info.machine
        print "These are supported:", sup_machine
        return False

    # check distro
    if sys_info.distro in SUPPORTED_SYSTEMS:

        # distro is supported, now verify the release
        if sys_info.codename not in SUPPORTED_SYSTEMS[sys_info.distro]:
            print "Operating system's release (%s) is not supported."\
                % sys_info.codename
            print "These are supported:", SUPPORTED_SYSTEMS[sys_info.distro].keys()
            return False

    # unsupported distro
    else:
        print "Operating system's distribution (%s) is not supported."\
            % sys_info.distro
        print "These are supported:", SUPPORTED_SYSTEMS.keys()
        return False

    MSGR.log("System check done... system is supported")
    return True

def create_data():
    '''Initializes data used by the script.
    Raises exception if something fails.'''

    global MSGR
    MSGR = Messenger()

    cmd = create_command(ARGS)

    sys_info = SysInfo()

    if not check_system(sys_info):
        raise FriendlyException("System check failed!")

    pkg_mgr = pkg_manager(sys_info)

    global IMPORTER
    IMPORTER = DynamicImporter(pkg_mgr)

    all_users = get_all_users()

    # filled if installing, otherwise time not wasted on downloading index file
    all_rootstraps = {}

    if cmd.is_install():
        all_rootstraps = get_all_rootstraps()

        global HAVE_ADDSW
        HAVE_ADDSW = SUPPORTED_SYSTEMS[sys_info.distro][sys_info.codename][SUS_HAS_ECLIPSE_IDE]

    return (cmd, sys_info, pkg_mgr, all_users, all_rootstraps)

def parse_options():
    '''Parses the command line options'''
    global OPTIONS
    global ARGS
    global OPT_PARSER

    usage = 'Usage: %prog [command] [options]' + \
'''

  %s.

  Installs/uninstalls %s.


Commands:
  %s\tInstall %s
  %s\tRemove (uninstall) %s
''' % (MY_NAME, PRODUCT_NAME, CmdNameInstall, PRODUCT_NAME, CmdNameRemove,
       PRODUCT_NAME)

    OPT_PARSER = OptionParser(usage = usage, version = "%prog 0.1.3 ($Revision: 1759 $)")

    OPT_PARSER.add_option("-g",
                          "--gui",
                          action = "store_true",
                          dest = "gui",
                          default = False,
                          help = "Run installer with GUI [default: %default]")

    OPT_PARSER.add_option("-m",
                          "--tools-mirror",
                          metavar = "URL",
                          dest = "tools_mirror",
                          default = "http://ftp.fi.debian.org/debian/",
                          help = "Tools distribution mirror [default: %default]. List of mirrors is available at http://www.debian.org/mirror/list")


    (OPTIONS, ARGS) = OPT_PARSER.parse_args()

def main():
    '''Main.'''

    parse_options()

    # this script must run with root privileges
    if os.geteuid() != 0:
        print "Root access is needed!"
        sys.exit(2)

    # try to create all objects
    try:
        (cmd, sys_info, pkg_mgr, all_users, all_rootstraps) = create_data()

    except FriendlyException, e:
        print_wrapped(str(e))
        MSGR.log_exc()
        sys.exit(1)

    print ("To follow the progress please view logs in %s\n" % (MSGR.fn_log,))

    # create and run the UI
    ui = create_ui()

    if ui:
        ui.run_cmd(cmd, pkg_mgr, all_users, all_rootstraps)

    sys.exit(0)

if __name__ == "__main__":
    main()
